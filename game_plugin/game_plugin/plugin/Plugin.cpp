//Includes
//Include AMS
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/plugin/Plugin.h"

//Include AMS Action
#include "game_plugin\action\GetUpWithMedikitAction.h"
#include "game_plugin\action\TreatStabilizeAction.h"
#include "game_plugin\action\OpenCloseDoorAction.h"
#include "game_plugin\action\PutPersonDownAction.h"
#include "game_plugin\action\PlacePersonAction.h"
#include "game_plugin\action\PlacePersonByFiremanAction.h"
#include "game_plugin\action\GetPersonFromVehicleAction.h"

//Include AMS Command
#include "game_plugin\command\BringBackAndEnterRDCommand.h"
#include "game_plugin\command\BringBackCommand.h"
#include "game_plugin\command\GetMedicBagCommand.h"
#include "game_plugin\command\TreatCommand.h"
#include "game_plugin\command\TreatFWCommand.h"
#include "game_plugin\command\TentPlacementCommand.h"
#include "game_plugin\command\TentDisplacementCommand.h"
#include "game_plugin\command\PutPersonDownCommand.h"
#include "game_plugin\command\PlacePersonByFiremanCommand.h"
#include "game_plugin\command\PlacePersonCommand.h"
#include "game_plugin\command\GetPersonFromVehicleCommand.h"
#include "game_plugin\command\PickupPersonByParamedicCommand.h"
#include "game_plugin\command\PickupPersonByCoronerCommand.h"

//Include AMS Component
#include "game_plugin\component\IndicatorComponent.h"
#include "game_plugin\component\TreatSkillLevelComponent.h"
#include "game_plugin\component\VehicleCategoryComponent.h"
#include "game_plugin\component\MedicalEquipmentComponent.h"
#include "game_plugin\component\SpecialInjuriesComponent.h"
#include "game_plugin\component\HospitalComponent.h"
#include "game_plugin\component\StatusIconComponent.h"

//Include AMS Game
#include "game_plugin\game\GameListener.h"

//Include em5
#include <em5/plugin/version/PluginVersion.h>
#include "em5\reflection\CampDefines.h"

//Include qsf
#include <qsf\map\Entity.h>
#include <qsf\localization\LocalizationSystem.h>
#include <qsf\log\LogSystem.h>

//Include qsf_game
#include <qsf_game\command\CommandManager.h>


//namespace AMS start
namespace AMS
{

	//public methods
	Plugin::Plugin() :
		qsf::Plugin(new em5::PluginVersion())
	{
	}

	//Protected virtual qsf::Plugin methods
	bool Plugin::onInstall()
	{
		try
		{
			//Action

			QSF_START_CAMP_CLASS_EXPORT(AMS::TreatStabilizeAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::GetUpWithMedikitAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::OpenCloseDoorAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PutPersonDownAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PlacePersonAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PlacePersonByFiremanAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::GetPersonFromVehicleAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			//Command

			QSF_START_CAMP_CLASS_EXPORT(AMS::BringBackAndEnterRDCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT
				
			QSF_START_CAMP_CLASS_EXPORT(AMS::BringBackCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::GetMedicBagCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::TreatCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::TreatFWCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::TentPlacementCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::TentDisplacementCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PutPersonDownCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PlacePersonByFiremanCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PlacePersonCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::GetPersonFromVehicleCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PickupPersonByParamedicCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::PickupPersonByCoronerCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			//Component

			QSF_CAMP_START_ENUM_EXPORT(AMS::MedicalEquipmentComponent::MedicalEquipment) //DropDown
				QSF_CAMP_ENUM_VALUE(Verbandskasten)
				QSF_CAMP_ENUM_VALUE(Notfallrucksack)
				QSF_CAMP_ENUM_VALUE(Sauerstoff)
				QSF_CAMP_ENUM_VALUE(Medikamente)
				QSF_CAMP_ENUM_VALUE(Defibrillator)
				QSF_CAMP_ENUM_VALUE(EKG)
				QSF_CAMP_ENUM_VALUE(Spineboard)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::MedicalEquipmentComponent, "Medical Equipment", "Create Medical Equipment")
				QSF_CAMP_IS_COMPONENT
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(Medical Equipment, AMS::MedicalEquipmentComponent::mMedicalEquipment, "Create Medical Equipment", AMS::MedicalEquipmentComponent::Verbandskasten)
			QSF_END_CAMP_CLASS_EXPORT

			QSF_CAMP_START_ENUM_EXPORT(AMS::SpecialInjuriesComponent::Adipositas) //DropDown
				QSF_CAMP_ENUM_VALUE(Adiposita_Active)
				QSF_CAMP_ENUM_VALUE(Adiposita_Deactive)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_CAMP_START_ENUM_EXPORT(AMS::SpecialInjuriesComponent::HWV) //DropDown
				QSF_CAMP_ENUM_VALUE(HWV_Active)
				QSF_CAMP_ENUM_VALUE(HWV_Deactive)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_CAMP_START_ENUM_EXPORT(AMS::SpecialInjuriesComponent::Intensiv) //DropDown
				QSF_CAMP_ENUM_VALUE(Intensiv_Active)
				QSF_CAMP_ENUM_VALUE(Intensiv_Deactive)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_CAMP_START_ENUM_EXPORT(AMS::SpecialInjuriesComponent::Psychological) //DropDown
				QSF_CAMP_ENUM_VALUE(Psychological_normal)
				QSF_CAMP_ENUM_VALUE(Psychological_light)
				QSF_CAMP_ENUM_VALUE(Psychological_medium)
				QSF_CAMP_ENUM_VALUE(Psychological_critical)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_CAMP_START_ENUM_EXPORT(AMS::SpecialInjuriesComponent::LNA_STATE) //DropDown
				QSF_CAMP_ENUM_VALUE(STATE_GREEN)
				QSF_CAMP_ENUM_VALUE(STATE_YELLOW)
				QSF_CAMP_ENUM_VALUE(STATE_RED)
				QSF_CAMP_ENUM_VALUE(STATE_DEAD)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::SpecialInjuriesComponent, "Special Injuries", QT_TR_NOOP("Do not use!"))
				QSF_CAMP_IS_COMPONENT
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(Adipositas, AMS::SpecialInjuriesComponent::mAdipositas, "Adipositas", AMS::SpecialInjuriesComponent::Adiposita_Deactive)
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(HWS, AMS::SpecialInjuriesComponent::mHWV, QT_TR_NOOP("Hals-Wirbels�ulen-Verletzung"), AMS::SpecialInjuriesComponent::HWV_Deactive)
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(Intensiv, AMS::SpecialInjuriesComponent::mIntensiv, QT_TR_NOOP("Intensiv Patient"), AMS::SpecialInjuriesComponent::Intensiv_Deactive)
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(Psychological, AMS::SpecialInjuriesComponent::mPsychological, QT_TR_NOOP("Psychological Damage"), AMS::SpecialInjuriesComponent::Psychological_normal)
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(LNA State, AMS::SpecialInjuriesComponent::mLNA_STATE, QT_TR_NOOP("LNA Category"), AMS::SpecialInjuriesComponent::STATE_GREEN)
			QSF_END_CAMP_CLASS_EXPORT

			QSF_CAMP_START_ENUM_EXPORT(AMS::VehicleCategoryComponent::VehicleCategory) //DropDown
				QSF_CAMP_ENUM_VALUE(Dummy)
				QSF_CAMP_ENUM_VALUE(NEF)
				QSF_CAMP_ENUM_VALUE(LNA)
				QSF_CAMP_ENUM_VALUE(ORG_L)
				QSF_CAMP_ENUM_VALUE(MTD)
				QSF_CAMP_ENUM_VALUE(LKW)
				QSF_CAMP_ENUM_VALUE(MTW)
				QSF_CAMP_ENUM_VALUE(KIT_MTW)
				QSF_CAMP_ENUM_VALUE(GW_SAN)
				QSF_CAMP_ENUM_VALUE(HvO)
				QSF_CAMP_ENUM_VALUE(KTW)
				QSF_CAMP_ENUM_VALUE(N_KTW)
				QSF_CAMP_ENUM_VALUE(RTW)
				QSF_CAMP_ENUM_VALUE(G_RTW)
				QSF_CAMP_ENUM_VALUE(S_RTW)
				QSF_CAMP_ENUM_VALUE(ITW)
				QSF_CAMP_ENUM_VALUE(NAW)
				QSF_CAMP_ENUM_VALUE(RTH)
				QSF_CAMP_ENUM_VALUE(I_RTH)
				QSF_CAMP_ENUM_VALUE(Zelt)
				QSF_CAMP_ENUM_VALUE(Leichenwagen)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::VehicleCategoryComponent, "Vehicle Categorys", "Set the Category of Vehicles")
				QSF_CAMP_IS_COMPONENT
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(Vehicle Category, AMS::VehicleCategoryComponent::mVehicleCategory, "Set the Category of Vehicles", AMS::VehicleCategoryComponent::Dummy)
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::HospitalComponent, "Hospital Departments", "Set the Departments of the Hospitals")
				QSF_CAMP_IS_COMPONENT
				QSF_ADD_CAMP_PROPERTY(Shockroom, AMS::HospitalComponent::getHasShockRoom, AMS::HospitalComponent::setHasShockRoom, "Schockraum", false)
				QSF_ADD_CAMP_PROPERTY(Ophthalmology, AMS::HospitalComponent::getHasOphthalmology, AMS::HospitalComponent::setHasOphthalmology, "Augenheilkunde", false)
				QSF_ADD_CAMP_PROPERTY(Surgery, AMS::HospitalComponent::getHasSurgery, AMS::HospitalComponent::setHasSurgery, QT_TR_NOOP("Chirugie Unfallchirugie Orthop�die"), false)
				QSF_ADD_CAMP_PROPERTY(ENTMedicine, AMS::HospitalComponent::getHasENTMedicine, AMS::HospitalComponent::setHasENTMedicine, "HNO Heilkunde", false)
				QSF_ADD_CAMP_PROPERTY(InternalMedicine, AMS::HospitalComponent::getHasInternalMedicine, AMS::HospitalComponent::setHasInternalMedicine, "Innere Medizin", false)
				QSF_ADD_CAMP_PROPERTY(Cardiology, AMS::HospitalComponent::getHasCardiology, AMS::HospitalComponent::setHasCardiology, "Kardiologie  Chest Pain Unit", false)
				QSF_ADD_CAMP_PROPERTY(Pediatry, AMS::HospitalComponent::getHasPediatry, AMS::HospitalComponent::setHasPediatry, "Kinder Heilkunde", false)
				QSF_ADD_CAMP_PROPERTY(Neurology, AMS::HospitalComponent::getHasNeurology, AMS::HospitalComponent::setHasNeurology, "Neurologie  Stroke Unit", false)
				QSF_ADD_CAMP_PROPERTY(Psychiatry, AMS::HospitalComponent::getHasPsychiatry, AMS::HospitalComponent::setHasPsychiatry, "Psychiatrie", false)
				QSF_ADD_CAMP_PROPERTY(Urology, AMS::HospitalComponent::getHasUrology, AMS::HospitalComponent::setHasUrology, "Urologie", false)
				QSF_ADD_CAMP_PROPERTY(Gynecology, AMS::HospitalComponent::getHasGynecology, AMS::HospitalComponent::setHasGynecology, QT_TR_NOOP("Gyn�kologie"), false)
				QSF_ADD_CAMP_PROPERTY(Infectiology, AMS::HospitalComponent::getHasInfectiology, AMS::HospitalComponent::setHasInfectiology, "Infektiologie", false)
			QSF_END_CAMP_CLASS_EXPORT

			QSF_CAMP_START_ENUM_EXPORT(AMS::TreatSkillLevelComponent::DoctorSkill) //DropDown
				QSF_CAMP_ENUM_VALUE(LNA)
				QSF_CAMP_ENUM_VALUE(ORG_L)
				QSF_CAMP_ENUM_VALUE(FirstAid)
				QSF_CAMP_ENUM_VALUE(Rettungssanitaeter)
				QSF_CAMP_ENUM_VALUE(Notfallsanitaeter)
				QSF_CAMP_ENUM_VALUE(Notarzt)
			QSF_CAMP_END_ENUM_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::TreatSkillLevelComponent, "Doctors Skill", "Set the Skill of Treatment of the Person")
				QSF_CAMP_IS_COMPONENT
				QSF_ADD_CAMP_PROPERTY_DIRECT_ACCESS(Skill Level, AMS::TreatSkillLevelComponent::mTreatSkill, "Set the Skill of Treatment of the Person", AMS::TreatSkillLevelComponent::FirstAid)
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(AMS::IndicatorComponent, QT_TR_NOOP("Indicator Component, Help Component for AMS"), QT_TR_NOOP("Do not use!"))
				QSF_CAMP_IS_COMPONENT
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(status_icon::StatusIconComponent, "StatusIconComponent", "Do not use!")
				QSF_CAMP_IS_COMPONENT
			QSF_END_CAMP_CLASS_EXPORT

			return true;
		}
		catch (const std::exception& e)
		{
			// Error!
			QSF_ERROR("Failed to install the plugin '" << getName() << "'. Exception caught: " << e.what(), QSF_REACT_NONE);
			return false;
		}
	}

	bool Plugin::onStartup()
	{
		mGameListener = new GameListener();
		mGameListener->init();

		return true;
	}

	void Plugin::onShutdown()
	{
		QSF_SAFE_DELETE(mGameListener);
	}

	void Plugin::onUninstall()
	{
		
	}

} //namespace AMS end
