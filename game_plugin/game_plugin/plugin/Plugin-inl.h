// Copyright (C) 2012-2015 Promotion Software GmbH


//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
namespace AMS
{


	//[-------------------------------------------------------]
	//[ Public methods                                        ]
	//[-------------------------------------------------------]
	inline Plugin::~Plugin()
	{
		// Nothing to do in here
	}


	//[-------------------------------------------------------]
	//[ Public virtual qsf::Plugin methods                    ]
	//[-------------------------------------------------------]
	inline const char* Plugin::getName() const
	{
		return "AMS Plugin";	// Please replace with a better name
	}

	inline const char* Plugin::getVendor() const
	{
		return "TH3LASTLINE / Niklas";	// Replace with your own name as author
	}

	inline const char* Plugin::getDescription() const
	{
		return "AMS Plugin Description";	// Insert a plugin description here
	}


//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
} // AMS
