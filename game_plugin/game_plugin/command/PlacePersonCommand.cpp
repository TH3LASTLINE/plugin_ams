//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\PlacePersonCommand.h"
#include "game_plugin\action\PlacePersonAction.h"
#include "game_plugin\action\OpenCloseDoorAction.h"

//Include em5
#include "em5\map\EntityHelper.h"
#include "em5\action\ActionPriority.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\action\base\PlacePersonAction.h"
#include "em5\action\ambulance\EnterRescueHeliAction.h"
#include "em5\game\targetpoint\EnterPassengerDoorTargetPointProvider.h"
#include "em5\game\targetpoint\EnterRescueHeliTargetPointProvider.h"
#include "em5\logic\local\ambulance\ParamedicLogic.h"
#include "em5/logic/local/ReserveLogic.h"
#include "em5/command/component/CommandableComponent.h"
#include "em5/component/vehicle/VehicleComponent.h"
#include "em5/component/vehicle/parts/RescueBusComponent.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\logic\gamelogic\GameLogicComponent.h"
#include "qsf\component\base\MetadataComponent.h"
#include <qsf/component/link/LinkComponent.h>
#include "qsf\map\Map.h"

//Include qsf_game
#include "qsf_game\command\CommandContext.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//Namespace AMS start
namespace AMS
{

	//Public definitions

	const uint32 PlacePersonCommand::PLUGINABLE_ID = qsf::StringHash("AMS::PlacePersonCommand");

	//private definitions

	const uint32 PlacePersonCommand::ACTION_PRIORITY = em5::action::COMMAND_HIGH;

	//public methods

	PlacePersonCommand::PlacePersonCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mMouseIconName = "place_person";
		mPriority = 205;
	}

	bool PlacePersonCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);
		if (!entityHelper.isCarryingAnotherPerson())
			return false;

		// Check if unit state is valid
		if (!entityHelper.isSquadPersonInValidState())
			return false;

		return true;
	}

	//Public virtual em5::Command methods

	bool PlacePersonCommand::checkAvailable()
	{
		return true;
	}

	bool PlacePersonCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriorityNoBlocking(caller, em5::action::BLOCKING))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool PlacePersonCommand::checkContext(const qsf::game::CommandContext& context)
	{
		qsf::Entity* targetEntity = context.mTargetEntity;
		if (nullptr == targetEntity || nullptr == context.mCaller)
			return false;

		{ 
			em5::CommandableComponent* callerCommandableComponent = context.mCaller->getComponent<em5::CommandableComponent>();
			em5::CommandableComponent* targetCommandableComponent = context.mTargetEntity->getComponent<em5::CommandableComponent>();
			if (nullptr != callerCommandableComponent && nullptr != targetCommandableComponent)
			{
				if (callerCommandableComponent->getPlayerId() != targetCommandableComponent->getPlayerId())
				{
					return false;
				}
			}
		}

		em5::EntityHelper entityHelper(*targetEntity);

		if (!entityHelper.isUnitInValidState())
			return false;

		if (entityHelper.isFlying())
			return false;

		em5::VehicleComponent* vehicleComponent = targetEntity->getComponent<em5::VehicleComponent>();
		if (nullptr == vehicleComponent)
			return false;

		if (!(vehicleComponent->isIntact() || vehicleComponent->isDamagedLevel1()))
			return false;

		const qsf::GameLogicComponent* gameLogicComponent = context.mCaller->getComponent<qsf::GameLogicComponent>();
		if (nullptr == gameLogicComponent)
			return false;

		const em5::ParamedicLogic* carryPersonLogic = gameLogicComponent->getGameLogic<em5::ParamedicLogic>();
		if (nullptr == carryPersonLogic)
			return false;

		qsf::Entity* carriedPersonEntity = context.mCaller->getMap().getEntityById(carryPersonLogic->getCarryPerson());
		if (carriedPersonEntity == nullptr)
			return false;

		em5::CommandableComponent* targetCommandableComponent = context.mTargetEntity->getComponent<em5::CommandableComponent>();
		if (!targetCommandableComponent->canTransportInjured())
			return false;
		
		if (!em5::EntityHelper(*carriedPersonEntity).checkVehicleHasFreeSeats(*targetEntity))
			return false;

		em5::CommandableComponent* commandableComponentCaller = context.mCaller->getComponent<em5::CommandableComponent>();
		if (nullptr != commandableComponentCaller && commandableComponentCaller->isAmbulanceParamedics())
		{
			if (!em5::EntityHelper(*context.mCaller).checkVehicleHasFreeSeats(*context.mTargetEntity))
				return false;
		}

		if (nullptr == context.mCaller->getOrCreateComponent<qsf::LinkComponent>())
			return false;

		qsf::LinkComponent* targetLinkComponent = targetEntity->getComponent<qsf::LinkComponent>();
		if (nullptr != targetLinkComponent)
		{
			em5::RescueBusComponent* rescueBusComponent = targetLinkComponent->getComponentFromEntityOrLinkedChild<em5::RescueBusComponent>();
			if (nullptr != rescueBusComponent && !rescueBusComponent->isExtended())
			{
				return false;
			}
		}

		return true;
	}

	void PlacePersonCommand::execute(const qsf::game::CommandContext& context)
	{
		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);		

		em5::CommandableComponent* commandableComponentTarget = context.mTargetEntity->getComponent<em5::CommandableComponent>();

		if (commandableComponentTarget && commandableComponentTarget->isAmbulanceHeli())
		{
			actionComponent.clearPlan();
			actionComponent.pushAction<em5::MoveAction>(100, qsf::action::INSERT_AT_FRONT).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::EnterRescueHeliTargetPointProvider::TARGET_POINT_ID));
			actionComponent.pushAction<em5::EnterRescueHeliAction>(200, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);
		}
		else if (context.mTargetEntity->getComponent<qsf::MetadataComponent>()->getName() == "seg_bhp_12_zelt")
		{
			actionComponent.clearPlan();
			actionComponent.pushAction<em5::MoveAction>(100, qsf::action::INSERT_AT_FRONT).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::EnterPassengerDoorTargetPointProvider::TARGET_POINT_ID));
			actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
			actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, true);
			actionComponent.pushAction<PlacePersonAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity);
			actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, false);
		}
		else if (context.mTargetEntity->getComponent<qsf::MetadataComponent>()->getName() == "ambulance_imt")
		{
			actionComponent.clearPlan();
			actionComponent.pushAction<em5::MoveAction>(100, qsf::action::INSERT_AT_FRONT).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::EnterPassengerDoorTargetPointProvider::TARGET_POINT_ID));
			actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
			actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, true);
			actionComponent.pushAction<PlacePersonAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity);
			actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, false);
		}
		else
		{
			actionComponent.clearPlan();
			actionComponent.pushAction<em5::MoveAction>(100, qsf::action::INSERT_AT_FRONT).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::EnterPassengerDoorTargetPointProvider::TARGET_POINT_ID));
			actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
			actionComponent.pushAction<em5::PlacePersonAction>(200, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);
		}
	}

}//Namespace AMS end