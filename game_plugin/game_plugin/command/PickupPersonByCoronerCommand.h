// Header Guard
#pragma once

// Includes
#include "em5/command/Command.h"

// Namespace AMS start
namespace AMS
{

	// Classes
	class PickupPersonByCoronerCommand : public em5::Command
	{

		// Public definitions
	public:
		static const uint32 PLUGINABLE_ID;

		// Public methods
	public:

		explicit PickupPersonByCoronerCommand(qsf::game::CommandManager* commandManager);

		bool checkCallerWithoutPriority(qsf::Entity& caller);

		// Public virtual em5::Command methods
	public:

		virtual bool checkAvailable() override;
		virtual bool checkCaller(qsf::Entity& caller) override;
		virtual bool checkContext(const qsf::game::CommandContext& context) override;
		virtual void execute(const qsf::game::CommandContext& context) override;

		// Private definitions
	private:

		static const uint32 ACTION_PRIORITY;

		// CAMP reflection system
		QSF_CAMP_RTTI()

	};

} // Namespace AMS end

  // CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::PickupPersonByCoronerCommand)