//Header guard
#pragma once

//Includes
//Include em5
#include "em5\command\Command.h"

//Namespace AMS start
namespace AMS
{

	//Classes
	class TentPlacementCommand : public em5::Command
	{

		//public definitions
		public:

			static const uint32 PLUGINABLE_ID;
			static const qsf::NamedIdentifier ACTION_ID;

		//public methods
		public:

			explicit TentPlacementCommand(qsf::game::CommandManager* commandManager);

			bool checkCallerWithoutPriority(qsf::Entity& caller);

		//Public virtual em5::command methods
		public:

			virtual bool checkAvailable() override;
			bool checkCaller(qsf::Entity& caller) override;
			virtual bool checkContext(const qsf::game::CommandContext& context) override;
			virtual void execute(const qsf::game::CommandContext& context) override;

		//private definitions
		private:

			static const uint32 ACTION_PRIORITY;

			glm::vec3 OldMousePos;
			glm::vec3 NewMousePos;

		//CAMP reflection System
			QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::TentPlacementCommand)