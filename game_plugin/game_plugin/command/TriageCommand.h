//Header guard
#pragma once

//Includes
#include "qsf\job\JobProxy.h"
#include "em5\command\Command.h"

//namespace AMS start
namespace AMS
{

	//Classes
	class TriageCommand : public em5::Command
	{

		//public definition
	public:

		static const uint32 PLUGINABLE_ID;

		//public methods
	public:

		explicit TriageCommand(qsf::game::CommandManager* commandManager);

		bool checkCallerWithoutPriority(qsf::Entity& caller);

		//public virtual em5::Command methods
	public:

		virtual bool checkAvailable() override;
		virtual bool checkCaller(qsf::Entity& caller) override;
		virtual bool checkContext(const qsf::game::CommandContext& context) override;
		virtual void execute(const qsf::game::CommandContext& context) override;

		static qsf::Entity* TriageCommand::ReturnClosestEntity(qsf::Entity* Doctor, std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> VectorList, float maxrange);
		static qsf::Entity* TriageCommand::ReturnClosestEntityInjured(qsf::Entity* Doctor);

		void updateButton();

		//private definitions
	private:

		qsf::JobProxy mUpdateButtonJob;

		static const uint32 ACTION_PRIORITY;

		//CAMP reflection system

		QSF_CAMP_RTTI()

	};

}//namespace AMS end

 //CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::TriageCommand)