//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\TriageCommand.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\map\EntityHelper.h"
#include "em5\game\targetpoint\GetEquipmentTargetPointProvider.h"
#include "em5\game\targetpoint\EnterPassengerDoorTargetPointProvider.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\action\equipment\GetEquipmentAction.h"
#include "em5\component\vehicle\VehicleComponent.h"
#include "em5\component\vehicle\RoadVehicleComponent.h"
#include "em5\component\vehicle\HelicopterComponent.h"
#include "em5\component\door\DoorComponent.h"
#include "em5\health\HealthComponent.h"
#include "em5\base\EquipmentAssets.h"
#include "em5\logic\HintHelper.h"
#include "em5\plugin\Messages.h"
#include "em5\game\Game.h"
#include "em5\game\selection\SelectionManager.h"
#include "em5\EM5Helper.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\map\EntityHelper.h"
#include "qsf\map\Map.h"
#include "qsf\QsfHelper.h"
#include "qsf\map\query\ComponentMapQuery.h"
#include "qsf\message\MessageManager.h"
#include "qsf\message\MessageSystem.h"
#include "qsf\log\LogSystem.h"
#include "qsf\plugin\QsfJobs.h"

//Include qsf_game
#include "qsf_game\equipment\InventoryComponent.h"
#include "qsf_game\equipment\EquipmentComponent.h"
#include "qsf_game\command\CommandContext.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//namespace AMS start
namespace AMS
{

	//public definitions

	const uint32 TriageCommand::PLUGINABLE_ID = qsf::StringHash("AMS::TriageCommand");

	//private definitions

	const uint32 TriageCommand::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods

	TriageCommand::TriageCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mShowAsGuiButton = true;
		mIconSettings.mButtonIconPath = "triage";

		mUpdateButtonJob.registerAt(qsf::QsfJobs::REALTIME, boost::bind(&TriageCommand::updateButton, this));
	}

	bool TriageCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);
		if (!entityHelper.isSquadPersonInValidState())
			return false;

		if (entityHelper.isCarryingAnotherPerson())
			return false;

		return true;
	}

	//public virtual em5::Command methods

	bool TriageCommand::checkAvailable()
	{
		return true;
	}

	bool TriageCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool TriageCommand::checkContext(const qsf::game::CommandContext& context)
	{
		if (!context.mAllowSelfExecution)
			return false;

		if (em5::EntityHelper(*context.mCaller).getActiveEquipmentEntity() != nullptr)
			return false;

		return true;
	}

	void TriageCommand::execute(const qsf::game::CommandContext& context)
	{

		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);

		//const em5::GetEquipmentAction* cheerAction = actionComponent.getAction<em5::GetEquipmentAction>();

		/*
		if (nullptr != cheerAction)
		{
			return;
		}*/

		qsf::Entity* ClosestEntity = nullptr;

		ClosestEntity = TriageCommand::ReturnClosestEntityInjured(context.mCaller);

		if (ClosestEntity == nullptr)
		{
			em5::HintHelper::showHint("Keine Person gefunden, die nicht triagiert wurde. Der Radius des LNAs betr�gt 50 Meter. Versuche eventuell mal n�her an untriagierte Verletzte Personen heranzugehen.", 0, qsf::Time::fromSeconds(0.0f));
			return;
		}

		//ActionPlan
		actionComponent.clearPlan();
		actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *ClosestEntity, em5::GetEquipmentTargetPointProvider::TARGET_POINT_ID));
		actionComponent.pushAction<em5::TurnToAction>(200, qsf::action::APPEND_TO_BACK).init(ClosestEntity->getId());
		
	}

	qsf::Entity* TriageCommand::ReturnClosestEntity(qsf::Entity* Doctor, std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> VectorList, float maxrange)
	{
		if (Doctor == nullptr)
			return nullptr;

		if (VectorList.empty())
			return nullptr;

		float Range = -1;
		qsf::Entity* ReturnedUnit = nullptr;

		for (auto Unit : VectorList)
		{
			if (Unit == nullptr)
				continue;

			float currentSquaredDistance = glm::distance2(em5::EntityHelper(*Doctor).getPosition(), em5::EntityHelper(*Unit).getPosition());

			if ((currentSquaredDistance < Range || Range == -1) && maxrange > currentSquaredDistance)
			{
				Range = currentSquaredDistance;
				ReturnedUnit = Unit;
			}
		}
		return ReturnedUnit;
	}

	qsf::Entity* TriageCommand::ReturnClosestEntityInjured(qsf::Entity* Doctor)
	{
		qsf::Entity* ClosesetEntity = nullptr;
		std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> vector;

		qsf::ComponentCollection::ComponentList<em5::HealthComponent> AllEntities = qsf::ComponentMapQuery(QSF_MAINMAP).getAllInstances<em5::HealthComponent>();

		for (auto Entity : AllEntities)
		{
			qsf::Entity* InjuredEntity = &Entity->getEntity();

			if (InjuredEntity == nullptr)
				continue;

			if (InjuredEntity->getComponent<em5::HealthComponent>()->isHealthy())
				continue;

			vector.push_back(InjuredEntity);
		}

		if (vector.empty())
		{
			return nullptr;
		}

		return (ClosesetEntity = TriageCommand::ReturnClosestEntity(Doctor, vector, 50));
	}

	void TriageCommand::updateButton()
	{
		em5::Game* game = em5::Game::getInstance();
		if (nullptr != game && game->isRunning())
		{
			std::vector<qsf::Entity*> selectedEntities;

			em5::SelectionManager& selectionManager = EM5_GAME.getSelectionManager();

			QSF_MAINMAP.getEntitiesByIds(selectionManager.getSelectedIdSet(), selectedEntities, true);

			// Show button only if all selected entities allow for the command
			bool showButton = true;

			for (qsf::Entity* caller : selectedEntities)
			{
				if (em5::EntityHelper(*caller).getActiveEquipmentEntity() != nullptr)
				{
					showButton = false;

					break;
				}
			}

			if (showButton != mIconSettings.mShowAsGuiButton)
			{
				mIconSettings.mShowAsGuiButton = showButton;

				QSF_MESSAGE.emitMessage(qsf::MessageConfiguration(em5::Messages::EM5_CHANGE_SELECTION));
			}
		}
	}

}//namespace AMS end