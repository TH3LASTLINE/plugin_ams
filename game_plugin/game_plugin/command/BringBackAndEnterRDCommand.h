//Header guard
#pragma once

//Includes
#include "em5\command\Command.h"

//namespace AMS start
namespace AMS
{

	//Classes
	class BringBackAndEnterRDCommand : public em5::Command
	{

		//public definitions
		public:

			static const uint32 PLUGINABLE_ID;

		//public methods
		public:

			explicit BringBackAndEnterRDCommand(qsf::game::CommandManager* commandManager);

			bool checkCallerWithoutPriority(qsf::Entity& caller);

		//public virtual em5::Command methods
		public:

			virtual bool checkAvailable() override;
			virtual bool checkCaller(qsf::Entity& caller) override;
			virtual bool checkContext(const qsf::game::CommandContext& context) override;
			virtual void execute(const qsf::game::CommandContext& context) override;

		//Private definitions
		private:

			static const uint32 ACTION_PRIORITY;

		//CAMP reflection system
			QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::BringBackAndEnterRDCommand)