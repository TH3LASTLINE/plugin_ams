//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\PlacePersonByFiremanCommand.h"
#include "game_plugin\action\PlacePersonByFiremanAction.h"
#include "game_plugin\action\OpenCloseDoorAction.h"

//Include em5
#include "em5\map\EntityHelper.h"
#include "em5\action\ActionPriority.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\game\targetpoint\EnterPassengerDoorTargetPointProvider.h"
#include "em5\logic\local\firefighters\FiremanLogic.h"
#include "em5/logic/local/ReserveLogic.h"
#include "em5/command/component/CommandableComponent.h"
#include "em5/component/vehicle/VehicleComponent.h"
#include "em5\health\HealthComponent.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\logic\gamelogic\GameLogicComponent.h"
#include "qsf\component\base\MetadataComponent.h"
#include <qsf/component/link/LinkComponent.h>
#include "qsf\map\Map.h"

//Include qsf_game
#include "qsf_game\command\CommandContext.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//Namespace AMS start
namespace AMS
{

	//Public definitions

	const uint32 PlacePersonByFiremanCommand::PLUGINABLE_ID = qsf::StringHash("AMS::PlacePersonByFiremanCommand");

	//private definitions

	const uint32 PlacePersonByFiremanCommand::ACTION_PRIORITY = em5::action::COMMAND_HIGH;

	//public methods

	PlacePersonByFiremanCommand::PlacePersonByFiremanCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mMouseIconName = "place_person";
		mPriority = 205;
	}

	bool PlacePersonByFiremanCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);
		if (!entityHelper.isPullingPerson())
			return false;

		// Check if unit state is valid
		if (!entityHelper.isSquadPersonInValidState())
			return false;

		return true;
	}

	//Public virtual em5::Command methods

	bool PlacePersonByFiremanCommand::checkAvailable()
	{
		return true;
	}

	bool PlacePersonByFiremanCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriorityNoBlocking(caller, em5::action::BLOCKING))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool PlacePersonByFiremanCommand::checkContext(const qsf::game::CommandContext& context)
	{
		qsf::Entity* targetEntity = context.mTargetEntity;
		if (nullptr == targetEntity || nullptr == context.mCaller)
			return false;

		{
			em5::CommandableComponent* callerCommandableComponent = context.mCaller->getComponent<em5::CommandableComponent>();
			em5::CommandableComponent* targetCommandableComponent = context.mTargetEntity->getComponent<em5::CommandableComponent>();
			if (nullptr != callerCommandableComponent && nullptr != targetCommandableComponent)
			{
				if (callerCommandableComponent->getPlayerId() != targetCommandableComponent->getPlayerId())
				{
					return false;
				}
			}
		}

		em5::EntityHelper entityHelper(*targetEntity);

		if (!entityHelper.isUnitInValidState())
			return false;

		if (entityHelper.isFlying())
			return false;

		em5::VehicleComponent* vehicleComponent = targetEntity->getComponent<em5::VehicleComponent>();
		if (nullptr == vehicleComponent)
			return false;

		if (!(vehicleComponent->isIntact() || vehicleComponent->isDamagedLevel1()))
			return false;

		const qsf::GameLogicComponent* gameLogicComponent = context.mCaller->getComponent<qsf::GameLogicComponent>();
		if (nullptr == gameLogicComponent)
			return false;

		const em5::FiremanLogic* carryPersonLogic = gameLogicComponent->getGameLogic<em5::FiremanLogic>();
		if (nullptr == carryPersonLogic)
			return false;

		qsf::Entity* carriedPersonEntity = context.mCaller->getMap().getEntityById(carryPersonLogic->getPullingPersonId());
		if (carriedPersonEntity == nullptr)
			return false;

		em5::CommandableComponent* targetCommandableComponent = context.mTargetEntity->getComponent<em5::CommandableComponent>();
		if (!targetCommandableComponent->canTransportInjured())
			return false;

		if (!em5::EntityHelper(*carriedPersonEntity).checkVehicleHasFreeSeats(*targetEntity))
			return false;

		em5::HealthComponent* mHC = carriedPersonEntity->getComponent<em5::HealthComponent>();
		if (mHC->isContaminated())
			return false;

		if (!mHC->isInjured())
			return false;

		if (nullptr == context.mCaller->getOrCreateComponent<qsf::LinkComponent>())
			return false;

		qsf::MetadataComponent* mMC = context.mTargetEntity->getComponent<qsf::MetadataComponent>();
		if (mMC->getName() != "zelt_seg_bhp_12_body")
			return false;

		return true;
	}

	void PlacePersonByFiremanCommand::execute(const qsf::game::CommandContext& context)
	{
		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);

		actionComponent.clearPlan();
		actionComponent.pushAction<em5::MoveAction>(100, qsf::action::INSERT_AT_FRONT).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::EnterPassengerDoorTargetPointProvider::TARGET_POINT_ID));
		actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
		actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, true);
		actionComponent.pushAction<PlacePersonByFiremanAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity);
		actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, false);		
	}

}//Namespace AMS end