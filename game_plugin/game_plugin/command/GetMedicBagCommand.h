//Header guard
#pragma once

//Includes
#include "qsf\job\JobProxy.h"
#include "em5\command\Command.h"

//namespace AMS start
namespace AMS
{

	//Classes
	class GetMedicBagCommand : public em5::Command
	{

		//public definition
		public:

			static const uint32 PLUGINABLE_ID;

		//public methods
		public:

			explicit GetMedicBagCommand(qsf::game::CommandManager* commandManager);

			bool checkCallerWithoutPriority(qsf::Entity& caller);

		//public virtual em5::Command methods
		public:

			virtual bool checkAvailable() override;
			virtual bool checkCaller(qsf::Entity& caller) override;
			virtual bool checkContext(const qsf::game::CommandContext& context) override;
			virtual void execute(const qsf::game::CommandContext& context) override;

			static qsf::Entity* GetMedicBagCommand::ReturnClosestEntity(qsf::Entity* Doctor, std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> VectorList, float maxrange = 9999);
			static qsf::Entity* GetMedicBagCommand::ReturnClosestEntityWithInventory(qsf::Entity* Doctor, std::string Inventory, std::string InventoryFullName);
			static bool GetMedicBagCommand::CheckVehicleForRightEquipment(qsf::Entity* Vehicle, std::string Inventory);

			void updateButton();

		//private definitions
		private:

			qsf::JobProxy mUpdateButtonJob;

			static const uint32 ACTION_PRIORITY;

		//CAMP reflection system

			QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::GetMedicBagCommand)