//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\PutPersonDownCommand.h"
#include "game_plugin\action\PutPersonDownAction.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\action\base\PlacePersonAction.h"
#include "em5\map\EntityHelper.h"
#include "em5\logic\local\base\CarryPersonLogic.h"

//Include qsf
#include "qsf\map\EntityHelper.h"
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\map\Map.h"

//Inlcude qsf_game
#include "qsf_game\command\CommandContext.h"

namespace AMS
{

	//public definitions

	const uint32 PutPersonDownCommand::PLUGINABLE_ID = qsf::StringHash("AMS::PutPersonDownCommand");

	//private definitions

	const uint32 PutPersonDownCommand::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods

	PutPersonDownCommand::PutPersonDownCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mPriority = 200;

		mIconSettings.mShowAsGuiButton = true;
		mIconSettings.mButtonIconPath = "drop_object";
	}

	bool PutPersonDownCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);

		if (!entityHelper.isSquadPersonInValidState())
			return false;

		return true;
	}

	//Public virtual em5::command methods

	bool PutPersonDownCommand::checkAvailable()
	{
		return true;
	}

	bool PutPersonDownCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool PutPersonDownCommand::checkContext(const qsf::game::CommandContext& context)
	{		
		em5::CarryPersonLogic* mCPL = em5::EntityHelper(context.mCaller).getGameLogic<em5::CarryPersonLogic>();

		uint64 mVictimID = mCPL->getCarryPerson();
		qsf::Map* mMap = &context.mCaller->getMap();
		qsf::Entity* mVictim = mMap->getEntityById(mVictimID);

		if (mVictim == nullptr)
		{
			return false;
		}

		if (!context.mAllowSelfExecution)
		{
			return false;
		}

		return true;
	}

	void PutPersonDownCommand::execute(const qsf::game::CommandContext& context)
	{
		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);

		actionComponent.clearPlan();
		actionComponent.pushAction<PutPersonDownAction>(100, qsf::action::APPEND_TO_BACK);
	}
}