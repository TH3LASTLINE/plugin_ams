//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\BringBackCommand.h"
#include "game_plugin\action\OpenCloseDoorAction.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\map\EntityHelper.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\action\base\PlayHintAction.h"
#include "em5\component\vehicle\RoadVehicleComponent.h"
#include "em5\base\EquipmentAssets.h"
#include "em5\action\equipment\GetEquipmentAction.h"
#include "em5\component\vehicle\HelicopterComponent.h"
#include "em5\game\targetpoint\GetEquipmentTargetPointProvider.h"
#include "em5\game\targetpoint\EnterPassengerDoorTargetPointProvider.h"
#include "em5\game\targetpoint\EnterTrunkDoorTargetPointProvider.h"
#include "em5\logic\HintHelper.h"
#include "em5\plugin\Messages.h"
#include "em5\game\Game.h"
#include "em5\game\selection\SelectionManager.h"
#include "em5\EM5Helper.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\map\EntityHelper.h"
#include "qsf\map\Map.h"
#include "qsf\QsfHelper.h"
#include "qsf\map\query\ComponentMapQuery.h"
#include "qsf\message\MessageManager.h"
#include "qsf\message\MessageSystem.h"
#include "qsf\plugin\QsfJobs.h"

//Include qsf_game
#include "qsf_game\equipment\InventoryComponent.h"
#include "qsf_game\equipment\EquipmentComponent.h"
#include "qsf_game\command\CommandContext.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//namespace AMS start
namespace AMS
{

	//public definitions

	const uint32 BringBackCommand::PLUGINABLE_ID = qsf::StringHash("AMS::BringBackCommand");

	//private definitions

	const uint32 BringBackCommand::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods

	BringBackCommand::BringBackCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mShowAsGuiButton = true;
		mIconSettings.mButtonIconPath = "drop_object";

		mUpdateButtonJob.registerAt(qsf::QsfJobs::REALTIME, boost::bind(&BringBackCommand::updateButton, this));
	}

	bool BringBackCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);
		if (!entityHelper.isSquadPersonInValidState())
			return false;

		if (entityHelper.isCarryingAnotherPerson())
			return false;

		return true;
	}

	//public virtual em5::Command methods

	bool BringBackCommand::checkAvailable()
	{
		return true;
	}

	bool BringBackCommand::checkCaller(qsf::Entity& caller)
	{

		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool BringBackCommand::checkContext(const qsf::game::CommandContext& context)
	{
		if (!context.mAllowSelfExecution)
			return false;

		if (em5::EntityHelper(*context.mCaller).getActiveEquipmentEntity() == nullptr)
			return false;

		return true;
	}

	void BringBackCommand::execute(const qsf::game::CommandContext& context)
	{

		//mIconSettings.mShowAsGuiButton = false;

		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);

		const em5::GetEquipmentAction* dummyAction = actionComponent.getAction<em5::GetEquipmentAction>();
		if (nullptr != dummyAction)
		{
			return;
		}

		qsf::Entity* ClosestEntity = nullptr;
		qsf::Entity* EquipmentEntity = em5::EntityHelper(*context.mCaller).getActiveEquipmentEntity();
		std::string EquipmentName = qsf::EntityHelper(*EquipmentEntity).getName();

		if (em5::EntityHelper(*context.mCaller).getActiveEquipmentEntity()->getComponent<qsf::game::EquipmentComponent>()->getEquipmentName() == "equipment_ambulance_medic_case_closed")
		{
			ClosestEntity = BringBackCommand::ReturnClosestEntityWithInventory(context.mCaller, "equipment_ambulance_medic_case_closed", "em5/prefab/equipment/equipment_ambulance_medic_case_closed");
		}
		else
		{
			ClosestEntity = BringBackCommand::ReturnClosestEntityWithInventory(context.mCaller, EquipmentName, EquipmentName);
		}

		if (ClosestEntity == nullptr)
		{
			em5::HintHelper::showHint("Kein Fahrzeug gefunden!", 0, qsf::Time::fromSeconds(0.0f));
			return;
		}

		if (em5::EntityHelper(*ClosestEntity).isRoadVehicle())
		{
			//ActionPlan
			actionComponent.clearPlan();
			actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *ClosestEntity, em5::GetEquipmentTargetPointProvider::TARGET_POINT_ID));
			actionComponent.pushAction<em5::TurnToAction>(200, qsf::action::APPEND_TO_BACK).init(ClosestEntity->getId());
			actionComponent.pushAction<em5::GetEquipmentAction>(200, qsf::action::APPEND_TO_BACK).init(*ClosestEntity, "");
		}
		else
		{
			//ActionPlan
			actionComponent.clearPlan();
			actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *ClosestEntity, em5::EnterPassengerDoorTargetPointProvider::TARGET_POINT_ID));
			actionComponent.pushAction<em5::TurnToAction>(200, qsf::action::APPEND_TO_BACK).init(ClosestEntity->getId());
			actionComponent.pushAction<OpenCloseDoorAction>().init(ClosestEntity, em5::DoorComponent::DOORTYPE_PASSENGER, true);
			actionComponent.pushAction<em5::GetEquipmentAction>(200, qsf::action::APPEND_TO_BACK).init(*ClosestEntity, "");
			actionComponent.pushAction<OpenCloseDoorAction>().init(ClosestEntity, em5::DoorComponent::DOORTYPE_PASSENGER, false);
		}

	}

	qsf::Entity* BringBackCommand::ReturnClosestEntity(qsf::Entity* Doctor, std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> VectorList, float maxrange)
	{

		if (Doctor == nullptr)
			return nullptr;

		if (VectorList.empty())
			return nullptr;

		float Range = -1;
		qsf::Entity* ReturnedUnit = nullptr;

		for (auto Unit : VectorList)
		{
			if (Unit == nullptr)
				continue;

			float currentSquaredDistance = glm::distance2(em5::EntityHelper(*Doctor).getPosition(), em5::EntityHelper(*Unit).getPosition());

			if ((currentSquaredDistance < Range || Range == -1) && maxrange > currentSquaredDistance)
			{
				Range = currentSquaredDistance;
				ReturnedUnit = Unit;
			}
		}

		return ReturnedUnit;
	}

	qsf::Entity* BringBackCommand::ReturnClosestEntityWithInventory(qsf::Entity* Doctor, std::string Inventory, std::string InventoryFullName)
	{

		qsf::Entity* ClosestEntity = nullptr;
		em5::RoadVehicleComponent* ClosestVehicle = em5::EntityHelper(*Doctor).getReachableRoadVehicleWithEquipment(Inventory);
		std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> a;

		if (ClosestVehicle != nullptr)
			a.push_back(&ClosestVehicle->getEntity());

		qsf::ComponentCollection::ComponentList<em5::HelicopterComponent> AllHelicopters = qsf::ComponentMapQuery(QSF_MAINMAP).getAllInstances<em5::HelicopterComponent>();

		for (auto Heli : AllHelicopters)
		{
			qsf::Entity* HeliEntity = &Heli->getEntity();

			if (HeliEntity == nullptr)
				continue;

			if (Heli->isFlying())
				continue;

			if (!BringBackCommand::CheckVehicleForRightEquipment(HeliEntity, InventoryFullName))
				continue;

			if (em5::EntityHelper(*HeliEntity).isSquadVehicleInValidState())
				a.push_back(HeliEntity);
		}

		if (a.empty())
			return nullptr;

		return (ClosestEntity = BringBackCommand::ReturnClosestEntity(Doctor, a));
	}

	bool BringBackCommand::CheckVehicleForRightEquipment(qsf::Entity* Vehicle, std::string Inventory)
	{
		qsf::game::InventoryComponent* IC = Vehicle->getComponent<qsf::game::InventoryComponent>();

		if (IC == nullptr)
			return false;

		for (size_t i = 0; i < IC->InventoryArray.size(); i++)
		{

			if (IC->InventoryArray.get(i) == Inventory)
				return true;

		}

		return false;
	}

	void BringBackCommand::updateButton()
	{
		em5::Game* game = em5::Game::getInstance();
		if (nullptr != game && game->isRunning())
		{
			std::vector<qsf::Entity*> selectedEntities;

			em5::SelectionManager& selectionManager = EM5_GAME.getSelectionManager();

			QSF_MAINMAP.getEntitiesByIds(selectionManager.getSelectedIdSet(), selectedEntities, true);

			// Show button only if all selected entities allow for the command
			bool showButton = true;

			for (qsf::Entity* caller : selectedEntities)
			{
				if (em5::EntityHelper(*caller).getActiveEquipmentEntity() == nullptr)
				{
					showButton = false;

					break;
				}
			}

			if (showButton != mIconSettings.mShowAsGuiButton)
			{
				mIconSettings.mShowAsGuiButton = showButton;

				QSF_MESSAGE.emitMessage(qsf::MessageConfiguration(em5::Messages::EM5_CHANGE_SELECTION));
			}
		}
	}

}//namespace AMS end