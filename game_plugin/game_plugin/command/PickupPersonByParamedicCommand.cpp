// Copyright (C) 2012-2016 Promotion Software GmbH


//[-------------------------------------------------------]
//[ Includes                                              ]
//[-------------------------------------------------------]
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/command/PickupPersonByParamedicCommand.h"

#include "em5/command/CommandContext.h"
#include "em5/health/HealthComponent.h"
#include "em5/logic/local/base/CarryPersonLogic.h"
#include "em5/action/ambulance/PickupPersonByParamedicAction.h"
#include "em5/action/ActionPriority.h"
#include "em5/action/move/MoveAction.h"
#include "em5/logic/local/ReserveLogic.h"
#include "em5/game/targetpoint/PickupPersonWithParamedicTargetPointProvider.h"
#include "em5/map/CollisionHelper.h"
#include "em5/map/EntityHelper.h"

#include <qsf_ai/navigation/goal/ReachObjectGoal.h>

#include <qsf/map/Map.h>
#include <qsf/logic/action/ActionComponent.h>


// Namespace AMS start
namespace AMS
{
	
	// Public definitions

	const uint32 PickupPersonByParamedicCommand::PLUGINABLE_ID = qsf::StringHash("AMS::PickupPersonByParamedicCommand");

	// Private definitions

	const uint32 PickupPersonByParamedicCommand::ACTION_PRIORITY = em5::action::COMMAND_STD;

	// Public methods

	PickupPersonByParamedicCommand::PickupPersonByParamedicCommand(qsf::game::CommandManager* commandManager) :
		Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mMouseIconName = "pickup_person_by_paramedic";
	}

	bool PickupPersonByParamedicCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		// Check if unit state is valid
		em5::EntityHelper entityHelper(caller);
		if (!entityHelper.isSquadPersonInValidState())
			return false;

		// The person can only carry one person
		if (entityHelper.isCarryingAnotherPerson())
			return false;

		return true;
	}

	// Public virtual em5::Command methods

	bool PickupPersonByParamedicCommand::checkAvailable()
	{
		return true;
	}

	bool PickupPersonByParamedicCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool PickupPersonByParamedicCommand::checkContext(const qsf::game::CommandContext& context)
	{
		// Target entity needed
		qsf::Entity* targetEntity = context.mTargetEntity;
		if (nullptr == targetEntity)
			return false;

		// Check if the target is in container
		em5::EntityHelper entityHelper(*targetEntity);
		if (nullptr != entityHelper.getContainerEntity())
			return false;

		// Check if target is hard linked
		if (entityHelper.isEntityHardLinked())
			return false;

		// Target needs to have a person component
		const em5::HealthComponent* healthComponent = targetEntity->getComponent<em5::HealthComponent>();
		if (nullptr == healthComponent)
			return false;

		// Target must be injured or dead
		if (healthComponent->getState() == em5::HealthComponent::STATE_HEALTHY || healthComponent->getState() == em5::HealthComponent::STATE_DEATHFIGHT || healthComponent->getState() == em5::HealthComponent::STATE_DEAD)
			return false;

		// Person must have in the injured state the maximum life energy
		if (healthComponent->getState() == em5::HealthComponent::STATE_INJURED && (healthComponent->getLifeEnergy() < healthComponent->getMaximumLifeEnergy()))
			return false;

		// Person must have a complete diagnosis to be carried
		if (healthComponent->isDiagnosisComplete() == false)
			return false;

		// Is target reserved
		const em5::CommandContext& em5CommandContext = static_cast<const em5::CommandContext&>(context);
		if (em5CommandContext.mIsAutomatism)
		{
			if (!em5::ReserveLogic::checkReservation(*targetEntity, context.mCaller->getId(), em5::ReserveLogic::MOVETO))
				return false;

			// Don't lead unit automatically in dangerous situations
			if (healthComponent->isReceivingFireDamage())
				return false;
		}

		// Check if target is potential reachable
		if (em5::CollisionHelper(targetEntity->getMap()).isInBlockedArea(*context.mCaller, *targetEntity))
			return false;

		return true;
	}

	void PickupPersonByParamedicCommand::execute(const qsf::game::CommandContext& context)
	{
		// Access the caller's action plan
		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);

		// Check if the same command with the same target is already in the action plan, return in this case without clearing the action plan
		const em5::PickupPersonByParamedicAction* pickupPersonByParamedicAction = actionComponent.getAction<em5::PickupPersonByParamedicAction>();
		if (nullptr != pickupPersonByParamedicAction)
		{
			if (pickupPersonByParamedicAction->getTargetEntityId() == context.mTargetEntity->getId())
			{
				// Don't push the same command twice
				return;
			}
		}

		// Clear plan before the actions are pushed
		actionComponent.clearPlan();
		
		actionComponent.pushAction<em5::MoveAction>().init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::PickupPersonWithParamedicTargetPointProvider::TARGET_POINT_ID));
		actionComponent.pushAction<em5::PickupPersonByParamedicAction>(em5::action::BLOCKING).init(*context.mTargetEntity);
	}

} // Namespace AMS end
