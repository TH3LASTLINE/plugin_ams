//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\BringBackAndEnterRDCommand.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\map\EntityHelper.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\action\equipment\GetEquipmentAction.h"
#include "em5\component\vehicle\RoadVehicleComponent.h"
#include "em5\base\EquipmentAssets.h"
#include "em5\component\door\DoorComponent.h"
#include "em5\action\base\EnterVehicleAction.h"

#include "em5\game\targetpoint\EnterCoDriverDoorTargetPointProvider.h"
#include "em5\game\targetpoint\EnterDriverDoorTargetPointProvider.h"
#include "em5\game\targetpoint\EnterPassengerDoorTargetPointProvider.h"
#include "em5\game\targetpoint\GetEquipmentTargetPointProvider.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//Include qsf_game
#include "qsf_game\equipment\InventoryComponent.h"
#include "qsf_game\equipment\EquipmentComponent.h"
#include "qsf_game\command\CommandContext.h"

//namespace AMS start
namespace AMS
{

	//public definitions

	const uint32 BringBackAndEnterRDCommand::PLUGINABLE_ID = qsf::StringHash("AMS::BringBackAndEnterRDCommand");

	//private definitions

	const uint32 BringBackAndEnterRDCommand::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods

	BringBackAndEnterRDCommand::BringBackAndEnterRDCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{

		mIconSettings.mMouseIconName = "enter_vehicle";
		mPriority = 212;

	}

	bool BringBackAndEnterRDCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{

		em5::EntityHelper entityHelper(caller);

		if (!entityHelper.isSquadPersonInValidState())
			return false;

		if (entityHelper.isCarryingAnotherPerson())
			return false;

		return true;
	}

	//Public virtual em5::Command methods

	bool BringBackAndEnterRDCommand::checkAvailable()
	{
		return true;
	}

	bool BringBackAndEnterRDCommand::checkCaller(qsf::Entity& caller)
	{

		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool BringBackAndEnterRDCommand::checkContext(const qsf::game::CommandContext& context)
	{

		if (!em5::EntityHelper(*context.mTargetEntity).isVehicle())
			return false;

		if (context.mTargetEntity == nullptr)
			return false;

		if (context.mCaller == context.mTargetEntity)
			return false;

		if (em5::EntityHelper(*context.mCaller).getActiveEquipmentEntity() == nullptr)
			return false;

		qsf::game::InventoryComponent* IC = context.mTargetEntity->getComponent<qsf::game::InventoryComponent>();

		if (IC == nullptr)
			return false;

		return true;
	}

	void BringBackAndEnterRDCommand::execute(const qsf::game::CommandContext& context)
	{

		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);

		const em5::GetEquipmentAction* dummyAction = actionComponent.getAction<em5::GetEquipmentAction>();

		if (nullptr != dummyAction)
			return;

		qsf::Entity* entity = context.mTargetEntity;

		//Action Plan
		actionComponent.clearPlan();
		actionComponent.pushAction<em5::MoveAction>(100, qsf::action::INSERT_AT_FRONT).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *entity, em5::GetEquipmentTargetPointProvider::TARGET_POINT_ID));
		actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::INSERT_AT_FRONT).init(entity->getId());
		actionComponent.pushAction<em5::GetEquipmentAction>(200, qsf::action::APPEND_TO_BACK).init(*entity, "");
		actionComponent.pushAction<em5::MoveAction>(200, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *entity, em5::EnterCoDriverDoorTargetPointProvider::TARGET_POINT_ID));
		actionComponent.pushAction<em5::TurnToAction>(200, qsf::action::APPEND_TO_BACK).init(entity->getId());
		actionComponent.pushAction<em5::EnterVehicleAction>(200, qsf::action::APPEND_TO_BACK).init(*entity, em5::DoorComponent::DOORTYPE_CODRIVER);

	}

}//namespace AMS end