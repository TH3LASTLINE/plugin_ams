//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\GetPersonFromVehicleCommand.h"
#include "game_plugin\action\GetPersonFromVehicleAction.h"
#include "game_plugin\action\OpenCloseDoorAction.h"

//Include em5
#include "em5\map\EntityHelper.h"
#include "em5\action\ActionPriority.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\action\base\PlacePersonAction.h"
#include "em5\action\ambulance\EnterRescueHeliAction.h"
#include "em5\game\targetpoint\EnterPassengerDoorTargetPointProvider.h"
#include "em5\game\targetpoint\EnterRescueHeliTargetPointProvider.h"
#include "em5\logic\local\ambulance\ParamedicLogic.h"
#include "em5/logic/local/ReserveLogic.h"
#include "em5/command/component/CommandableComponent.h"
#include "em5/component/vehicle/VehicleComponent.h"
#include "em5/component/vehicle/parts/RescueBusComponent.h"
#include "em5\base\ContainerCategory.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\logic\gamelogic\GameLogicComponent.h"
#include "qsf\component\base\MetadataComponent.h"
#include <qsf/component/link/LinkComponent.h>
#include "qsf\map\Map.h"
#include "qsf\link\LinkProxy.h"
#include "qsf\link\link\prototype\ContainerLink.h"

//Include qsf_game
#include "qsf_game\command\CommandContext.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//Namespace AMS start
namespace AMS
{

	//Public definitions

	const uint32 GetPersonFromVehicleCommand::PLUGINABLE_ID = qsf::StringHash("AMS::GetPersonFromVehicleCommand");

	//private definitions

	const uint32 GetPersonFromVehicleCommand::ACTION_PRIORITY = em5::action::COMMAND_HIGH;

	//public methods

	GetPersonFromVehicleCommand::GetPersonFromVehicleCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mMouseIconName = "victim";
		mPriority = 205;
	}

	bool GetPersonFromVehicleCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);
		if (entityHelper.isCarryingAnotherPerson())
			return false;

		// Check if unit state is valid
		if (!entityHelper.isSquadPersonInValidState())
			return false;

		return true;
	}

	//Public virtual em5::Command methods

	bool GetPersonFromVehicleCommand::checkAvailable()
	{
		return true;
	}

	bool GetPersonFromVehicleCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriorityNoBlocking(caller, em5::action::BLOCKING))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool GetPersonFromVehicleCommand::checkContext(const qsf::game::CommandContext& context)
	{
		qsf::Entity* targetEntity = context.mTargetEntity;
		if (nullptr == targetEntity || nullptr == context.mCaller)
			return false;

		{
			em5::CommandableComponent* callerCommandableComponent = context.mCaller->getComponent<em5::CommandableComponent>();
			em5::CommandableComponent* targetCommandableComponent = context.mTargetEntity->getComponent<em5::CommandableComponent>();
			if (nullptr != callerCommandableComponent && nullptr != targetCommandableComponent)
			{
				if (callerCommandableComponent->getPlayerId() != targetCommandableComponent->getPlayerId())
				{
					return false;
				}
			}
		}

		em5::EntityHelper entityHelper(*targetEntity);

		if (!entityHelper.isUnitInValidState())
			return false;

		if (entityHelper.isFlying())
			return false;

		em5::CommandableComponent* targetCommandableComponent = context.mTargetEntity->getComponent<em5::CommandableComponent>();
		if (!targetCommandableComponent->canTransportInjured())
			return false;

		em5::VehicleComponent* vehicleComponent = targetEntity->getComponent<em5::VehicleComponent>();
		if (nullptr == vehicleComponent)
			return false;

		qsf::LinkComponent* targetLinkComponent = targetEntity->getComponent<qsf::LinkComponent>();
		if (nullptr != targetLinkComponent)
		{
			em5::RescueBusComponent* rescueBusComponent = targetLinkComponent->getComponentFromEntityOrLinkedChild<em5::RescueBusComponent>();
			if (nullptr != rescueBusComponent && !rescueBusComponent->isExtended())
			{
				return false;
			}
		}

		//

		em5::VehicleComponent* mVehicleComponent = context.mTargetEntity->getComponent<em5::VehicleComponent>();
		const qsf::LinkProxy* wrongLinkProxyPointer = &mVehicleComponent->getLinkProxy();
		const qsf::LinkProxy* rightLinkProxyPointer = reinterpret_cast<const qsf::LinkProxy*>(reinterpret_cast<const char*>(wrongLinkProxyPointer) - 8);
		auto& links = reinterpret_cast<const boost::container::flat_map<qsf::LinkAnchorId, qsf::ContainerLink*>&>(rightLinkProxyPointer->getLinkConnectionMap());

		std::vector<qsf::Entity*> mVec;

		for (auto iterator : links)
		{
			const qsf::ContainerLink& containerLink = *iterator.second;

			if (containerLink.mContainerCategory == em5::container::CONTAINERTYPE_PASSENGER)
			{
				mVec.push_back(&containerLink.getTargetEntity());
			}
		}

		if (mVec.size() < 1)
			return false;

		return true;
	}

	void GetPersonFromVehicleCommand::execute(const qsf::game::CommandContext& context)
	{
		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);

		em5::CommandableComponent* commandableComponentTarget = context.mTargetEntity->getComponent<em5::CommandableComponent>();

		actionComponent.clearPlan();
		actionComponent.pushAction<em5::MoveAction>(100, qsf::action::INSERT_AT_FRONT).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::EnterPassengerDoorTargetPointProvider::TARGET_POINT_ID));
		actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
		actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, true);
		actionComponent.pushAction<GetPersonFromVehicleAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity);
		actionComponent.pushAction<OpenCloseDoorAction>(200, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity, em5::DoorComponent::DOORTYPE_PASSENGER, false);
	}

}//Namespace AMS end