//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\TreatFWCommand.h"
#include "game_plugin\component\TreatSkillLevelComponent.h"
#include "game_plugin\action\TreatStabilizeAction.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\map\EntityHelper.h"
#include "em5\health\HealthComponent.h"
#include "em5\health\injury\Injury.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\action\base\PlayHintAction.h"
#include "em5\action\base\PlayAudioAction.h"
#include "em5\action\ambulance\TreatPersonAction.h"
#include "em5\game\targetpoint\TreatPersonTargetPointProvider.h"
#include "em5\logic\local\ReserveLogic.h"
#include "em5\map\EntityHelper.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\log\LogSystem.h"
#include "qsf\localization\LocalizationSystem.h"
#include "qsf\map\EntityHelper.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//Include qsf_game
#include "qsf_game\command\CommandContext.h"
#include "qsf_game\equipment\EquipmentComponent.h"

//namespace AMS start
namespace AMS
{
	//public definitions

	const uint32 TreatFWCommand::PLUGINABLE_ID = qsf::StringHash("AMS::TreatFWCommand");

	//private definitions

	const uint32 TreatFWCommand::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods

	TreatFWCommand::TreatFWCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mMouseIconName = "treat_person";
		mPriority = 200;
	}

	bool TreatFWCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);

		if (!entityHelper.isSquadPersonInValidState())
			return false;

		if (entityHelper.isCarryingAnotherPerson())
			return false;

		return true;
	}

	//Public virtual em5::command methods

	bool TreatFWCommand::checkAvailable()
	{
		return true;
	}

	bool TreatFWCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool TreatFWCommand::checkContext(const qsf::game::CommandContext& context)
	{
		if (!em5::ReserveLogic::checkReservation(*context.mTargetEntity, context.mCaller->getId()))
		{
			return false;
		}

		if (!em5::EntityHelper(*context.mTargetEntity).isPersonInjured())
			return false;

		if (em5::EntityHelper(*context.mCaller).getActiveEquipmentEntity() == nullptr)
		{
			return false;
		}

		if (em5::EntityHelper(*context.mCaller).getActiveEquipmentEntity()->getComponent<qsf::game::EquipmentComponent>()->getEquipmentName() != "equipment_ambulance_medic_case_closed")
		{
			return false;
		}

		em5::HealthComponent* healthcomponent = context.mTargetEntity->getComponent<em5::HealthComponent>();

		if (healthcomponent == nullptr)
			return false;

		return true;
	}

	void TreatFWCommand::execute(const qsf::game::CommandContext& context)
	{
		em5::HealthComponent* healthcomponent = context.mTargetEntity->getComponent<em5::HealthComponent>();
		qsf::ActionComponent& actionComponent = getActionComponent(*context.mCaller);
		const TreatStabilizeAction* cheerAction = actionComponent.getAction<TreatStabilizeAction>();

		if (nullptr != cheerAction)
		{
			return;
		}

		actionComponent.clearPlan();

		AMS::TreatSkillLevelComponent* Component = context.mCaller->getComponent<AMS::TreatSkillLevelComponent>();	//For Reanimation

		if (healthcomponent->getLifeEnergy() < 50)
		{
			actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
			actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
			actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

			em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
			em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
			em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
			em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS, true);

			Component->starttracker(context.mTargetEntity->getId());
			return;
		}
		if (healthcomponent->getLifeEnergy() > 500)
		{
			switch (Component->mTreatSkill)
			{
			case AMS::TreatSkillLevelComponent::Rettungssanitaeter:
			{
				if (TreatFWCommand::SetInjuryLevel(healthcomponent->getInjury()->getName()) == 0)
				{
					actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
					actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
					actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
					em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
				}
				else if (TreatFWCommand::SetInjuryLevel(healthcomponent->getInjury()->getName()) == 1)
				{
					actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
					actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
					actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
					em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
				}
				else
				{
					actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
					actionComponent.pushAction<em5::PlayHintAction>(200, qsf::action::APPEND_TO_BACK).init(QT_TR_NOOP("AMS_HINT_DOCTOR_01"), em5::HintMessage::MessageType::MESSAGETYPE_HINT);
					actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
					actionComponent.pushAction<TreatStabilizeAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
					em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS, true);
				}
			}
			case AMS::TreatSkillLevelComponent::Notfallsanitaeter:
			{
				if (TreatFWCommand::SetInjuryLevel(healthcomponent->getInjury()->getName()) == 0)
				{
					actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
					actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
					actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
					em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
				}
				else if (TreatFWCommand::SetInjuryLevel(healthcomponent->getInjury()->getName()) == 1)
				{
					actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
					actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
					actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
					em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
				}
				else if (TreatFWCommand::SetInjuryLevel(healthcomponent->getInjury()->getName()) == 2)
				{
					actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
					actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
					actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
					em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
				}
				else
				{
					actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
					actionComponent.pushAction<em5::PlayHintAction>(200, qsf::action::APPEND_TO_BACK).init(QT_TR_NOOP("AMS_HINT_DOCTOR_01"), em5::HintMessage::MessageType::MESSAGETYPE_HINT);
					actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
					actionComponent.pushAction<TreatStabilizeAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
					em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
					em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS, true);
				}
			}
			case AMS::TreatSkillLevelComponent::Notarzt:
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
				actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
			}
			case AMS::TreatSkillLevelComponent::LNA:
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
				actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
			}
			}
		}
		if (healthcomponent->getLifeEnergy() < 500)
		{
			if (Component->mTreatSkill == AMS::TreatSkillLevelComponent::Notarzt)
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
				actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
			}
			else if (Component->mTreatSkill == AMS::TreatSkillLevelComponent::LNA)
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
				actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
			}
			else
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(*context.mCaller, *context.mTargetEntity, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::PlayHintAction>(200, qsf::action::APPEND_TO_BACK).init(QT_TR_NOOP("AMS_HINT_DOCTOR_01"), em5::HintMessage::MessageType::MESSAGETYPE_HINT);
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(context.mTargetEntity->getId());
				actionComponent.pushAction<TreatStabilizeAction>(100, qsf::action::APPEND_TO_BACK).init(*context.mTargetEntity);

				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*context.mTargetEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS, true);
			}
		}		
	}

	int TreatFWCommand::SetInjuryLevel(std::string Injury)	//All Injuries of the vanilla game
	{
		int mInjuryLevel;
		if (Injury == "Burn_3_Grade") mInjuryLevel = 1;
		else if (Injury == "Burn_4_Grade") mInjuryLevel = 3;
		else if (Injury == "Burn_Of_Breath") mInjuryLevel = 3;    // Verbrannte Atemwege
		else if (Injury == "Smoke_Poisoning") mInjuryLevel = 1;   // "Rauchgasvergiftung"
		else if (Injury == "Broken_Arm") mInjuryLevel = 2;   // "Knochenbruch Arm"
		else if (Injury == "Broken_Leg") mInjuryLevel = 2;  // "Knochenbruch Bein"	
		else if (Injury == "Broken_Skull") mInjuryLevel = 3;  // "Schädelbasisbruch"
		else if (Injury == "Brain_Bleeding") mInjuryLevel = 3;   // "Hirnblutung"
		else if (Injury == "Inner_Bleeding") mInjuryLevel = 3;  // "Innere Blutungen"
		else if (Injury == "Lost_Arm") mInjuryLevel = 3;   // "Arm amputiert"
		else if (Injury == "Lost_Leg") mInjuryLevel = 3;   // "Bein amputiert"
		else if (Injury == "Whiplash") mInjuryLevel = 1;   // "Schleudertrauma"
		else if (Injury == "gun_shot_wound_lung") mInjuryLevel = 3;   // "Offene Schusswunde, Lungenflügel perforiert"
		else if (Injury == "gun_shot_wound_Body") mInjuryLevel = 1;   // "Durchschuss (Rumpf), stark blutend"
		else if (Injury == "gun_shot_wound_Body_hard") mInjuryLevel = 2;   // "Durchschuss (Rumpf), innere Blutungen"
		else if (Injury == "stab_wound_lung") mInjuryLevel = 3;   // "Offene Stichwunde, Lungenflügel perforiert"
		else if (Injury == "stab_wound_body") mInjuryLevel = 1;   // "Offene Stichwunde (Rumpf), stark blutend"
		else if (Injury == "stab_wound_body_inner_bleeding") mInjuryLevel = 2;  // "Offene Stichwunde, innere Blutungen"
		else if (Injury == "bite_wound") mInjuryLevel = 2;  // "Offene Bisswunden, Muskel- und Nervengewebe verletzt"
		else if (Injury == "bite_wound_bleeding") mInjuryLevel = 1;  // "Offene Bisswunden, Arterie verletzt"
		else if (Injury == "Head_body_laceration") mInjuryLevel = 1;	//	"Platzwunden an Körper und Kopf"
		else if (Injury == "electric_shock") mInjuryLevel = 3;  // "Lähmung des Herzens als Folge eines schweren elektrischen Schlags"
		else if (Injury == "Shock") mInjuryLevel = 2;	//	"Schockzustand (nach Schaltkasten oftmals...)"
		else if (Injury == "Weakness") mInjuryLevel = 2;  // "Entkräftung"
		else if (Injury == "Water_Filled_Lung") mInjuryLevel = 3;  // "Wasser in der Lunge, Atemstillstand"
		else if (Injury == "HeartAttack") mInjuryLevel = 3;  // "Herzinfarkt"
		else if (Injury == "Stroke") mInjuryLevel = 3;  // "Schlaganfall"
		else if (Injury == "CirculatoryCollapse") mInjuryLevel = 1;  // "Kreislaufkollaps"
		else if (Injury == "Food_Poisoning") mInjuryLevel = 2;  // "Nahrungsmittelvergiftung"
		else if (Injury == "Alcohol_Poisoning") mInjuryLevel = 1;  // "Alkoholvergiftung"
		else if (Injury == "Low_Blood_Pressure") mInjuryLevel = 2;  // "Niedriger Bludruck, Patient zusammengebrochen"
		else if (Injury == "Angina_Pectoris") mInjuryLevel = 2;  // "Angina Pectoris"
		else if (Injury == "Shortage_Of_Breath") mInjuryLevel = 3;  // "Starke Atemnot durch allergische Reaktion"
		else if (Injury == "Collapsed_lung") mInjuryLevel = 3; // "Lungenriss"
		else if (Injury == "Hypothermia") mInjuryLevel = 3; // "Schwere Unterkühlung, Kreislaufstillstand"
		else if (Injury == "Avian_Influenza") mInjuryLevel = 1;  // "Vogelgrippe (Influenza): Patient hat Lebensenergie, steht"
		else if (Injury == "Avian_Influenza_Injured") mInjuryLevel = 2;  // "Vogelgrippe (Influenza): Patient hat Verletztenenergie, liegt"
		else if (Injury == "Swine_Influenza") mInjuryLevel = 1;  // "Schweinegrippe: Patient hat Lebensenergie, steht"
		else if (Injury == "Swine_Influenza_Injured") mInjuryLevel = 2;  // "Schweinegrippe: Patient hat Verletztenenergie, liegt"	
		else if (Injury == "Hard_Asthma") mInjuryLevel = 1;  // "Schwerer Asthmaanfall: Patient hat Lebensenergie, steht"
		else if (Injury == "Hard_Asthma_Injured") mInjuryLevel = 2;  // "Schwerer Asthmaanfall: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Hydrophobia") mInjuryLevel = 1;  // "Gehirnentzündung durch Tollwut: Patient hat Lebensenergie, steht"
		else if (Injury == "Hydrophobia_Injured") mInjuryLevel = 2;  // "Gehirnentzündung durch Tollwut: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Chemical_Contamination") mInjuryLevel = 2;  // "Chemische Kontamination: Patient hat Lebensenergie, steht"
		else if (Injury == "Chemical_Contamination_Shortage_Of_Breath") mInjuryLevel = 3;  // "Schwere Atemnot wegen chemischer Kontamination: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Chemical_Contamination_Breath_Burn") mInjuryLevel = 3;  // "Verätzung der Atemwege wegen chemischer Kontamination: Patient hat Verletztenenergie, liegt"
		else if (Injury == "Radioactive_Contamination") mInjuryLevel = 2;  // "Radioaktive Kontamination: Patient hat Lebensenergie, steht"
		else if (Injury == "Radioactive_Contamination_Organ_Bleeding") mInjuryLevel = 3;  // "Organblutung als Folge einer radioaktiven Kontamination"
		else if (Injury == "Radioactive_Contamination_Organ_Failure") mInjuryLevel = 3;  // "Organversagen als Folge einer radioaktiven Kontamination "
		else if (Injury == "Plague_Contamination") mInjuryLevel = 2;		// "Pestkrankheit"
		else if (Injury == "Plague_Contamination_Positiv") mInjuryLevel = 2;			// "Pestkrankheit quick test positiv"
		else if (Injury == "Plague_Contamination_Negativ") mInjuryLevel = 1;			// "Pestkrankheit quick test negativ"
		else if (Injury == "Drowning_normal") mInjuryLevel = 3;  // "Ertrinken normal"

		else mInjuryLevel = 3; //call the doctor :D
		return mInjuryLevel;
	}

}//namespace AMS end