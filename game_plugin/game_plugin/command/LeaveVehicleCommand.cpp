//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\LeaveVehicleCommand.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\map\EntityHelper.h"

//Include qsf

//Include qsf_game

//Namespace AMS start
namespace AMS
{

	//public definitions

	const uint32 LeaveVehicleCommand::PLUGINABLE_ID = qsf::StringHash("AMS::LeaveVehicleCommand");

	//private definitions

	const uint32 LeaveVehicleCommand::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods

	LeaveVehicleCommand::LeaveVehicleCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mIconSettings.mMouseIconName = "exit_vehicle";
		mPriority = 90;
	}

	bool LeaveVehicleCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		em5::EntityHelper entityHelper(caller);

		if (!entityHelper.isSquadPersonInValidState())
			return false;

		if (entityHelper.isCarryingAnotherPerson())
			return false;

		return true;
	}

	bool LeaveVehicleCommand::checkAvailable()
	{
		return true;
	}

	bool LeaveVehicleCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool LeaveVehicleCommand::checkContext(const qsf::game::CommandContext& context)
	{
		return true;
	}

	void LeaveVehicleCommand::execute(const qsf::game::CommandContext& context)
	{

	}

}