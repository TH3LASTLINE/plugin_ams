//Header guard
#pragma once

//Includes
#include "qsf\job\JobProxy.h"
#include "qsf\message\MessageProxy.h"

//Namespace AMS start
namespace AMS
{

	//Classes

	class GameListener
	{

		//public methods

		public:

			GameListener();

			virtual ~GameListener();

			void init();

		//private methods

		private:

			void startup(const qsf::MessageParameters& parameters);
			void shutdown(const qsf::MessageParameters& parameters);

			void ShowHealthState(const qsf::MessageParameters& parameters);
			
		//private data

		private:

			qsf::MessageProxy mStartupMessageProxy;
			qsf::MessageProxy mShutdownMessageProxy;

			bool mCanStart;

			qsf::MessageProxy	mShowHealthState;
	};

}//namespace AMS end