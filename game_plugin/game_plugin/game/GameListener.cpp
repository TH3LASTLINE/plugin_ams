//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\game\GameListener.h"
#include "game_plugin\component\StatusIconComponent.h"

//Include em5
#include "em5\debug\GameDebugGroup.h"
#include "em5\plugin\Messages.h"
#include "em5\EM5Helper.h"
#include "em5\health\HealthComponent.h"
#include "em5\gui\debug\DebugGuiWindow.h"
#include <em5/gui/EmergencyGui.h>
#include "em5\gui\IngameHud.h"

//Include qsf
#include "qsf\message\MessageConfiguration.h"
#include "qsf\QsfHelper.h"
#include "qsf\map\Map.h"
#include "qsf\map\Entity.h"
#include <qsf/gui/GuiContext.h>

//Include sonstiges
#include <Rocket/Debugger/Debugger.h>
//#include "status_icon\component\StatusIconComponent.h"
#include "game_plugin\component\StatusIconComponent.h"

//namespace AMS start
namespace AMS
{

	//public methods
	GameListener::GameListener() :
		mCanStart(true)
	{
		
	}

	GameListener::~GameListener()
	{
		mStartupMessageProxy.unregister();
		mShutdownMessageProxy.unregister();
	}

	void GameListener::init()
	{
		mStartupMessageProxy.registerAt(qsf::MessageConfiguration(em5::Messages::GAME_STARTUP_FINISHED), boost::bind(&GameListener::startup, this, _1));
		mShutdownMessageProxy.registerAt(qsf::MessageConfiguration(em5::Messages::GAME_SHUTDOWN_STARTING), boost::bind(&GameListener::shutdown, this, _1));
	}

	void GameListener::startup(const qsf::MessageParameters& parameters)
	{
		mShowHealthState.registerAt(qsf::MessageConfiguration(em5::Messages::EM5_COMPONENT_GETSINJURED_PERSON), boost::bind(&GameListener::ShowHealthState, this, _1));
	}

	void GameListener::shutdown(const qsf::MessageParameters& parameters)
	{
		mShowHealthState.unregister();
	}

	void GameListener::ShowHealthState(const qsf::MessageParameters& parameters)
	{
		qsf::Entity* InjuredEntity = QSF_MAINMAP.getEntityById(parameters.getConfiguration().getFilter(1));

		em5::StatusOverlayComponent::hideIcon(*InjuredEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
		em5::StatusOverlayComponent::hideIcon(*InjuredEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);

		em5::StatusOverlayComponent::showIcon(*InjuredEntity, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS, true);
	}

}//namespace AMS end