//Header guard
#pragma once

//Includes 
//Include AMS
#include "game_plugin\Export.h"

//Include qsf
#include "qsf\component\Component.h"
#include "qsf\job\JobProxy.h"
#include "qsf\debug\DebugDrawProxy.h"
#include "qsf\message\MessageProxy.h"

//namespace AMS start
namespace AMS
{

	//Classes
	class TreatSkillLevelComponent : public qsf::Component
	{

		//public definitions
		public:

			static const uint32 COMPONENT_ID;

		//public methods
		public:

			TreatSkillLevelComponent(qsf::Prototype* prototype);

			~TreatSkillLevelComponent();

			enum DoctorSkill	//Can be changed, only visible in the Editor
			{
				LNA,
				ORG_L,
				FirstAid,
				Rettungssanitaeter,
				Notfallsanitaeter,
				Notarzt
			};

			DoctorSkill mTreatSkill;


			void starttracker(uint64 targetEntity);

		//protected virtual qsf::Component methods
		protected:

			//Lifecycle

			virtual bool onStartup() override;
			virtual void onShutdown() override;

		//private methods
		private:

			void updateJob(const qsf::JobArguments& jobArguments);

		//private data
		private:

			qsf::JobProxy mJobProxy;
			qsf::Time mTimePassed;

			uint64 TargetEntity;

		//CAMP reflection system
			QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE(AMS::TreatSkillLevelComponent::DoctorSkill)
QSF_CAMP_TYPE_NONCOPYABLE(AMS::TreatSkillLevelComponent)