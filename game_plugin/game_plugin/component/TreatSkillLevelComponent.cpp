//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\component\TreatSkillLevelComponent.h"
#include "game_plugin\action\TreatStabilizeAction.h"
#include "game_plugin\action\GetUpWithMedikitAction.h"

//Include em5
#include "em5\plugin\Messages.h"
#include "em5\health\HealthComponent.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\action\base\PlayHintAction.h"
#include "em5\action\base\PlayAudioAction.h"
#include "em5\action\ambulance\TreatPersonAction.h"
#include "em5\game\targetpoint\TreatPersonTargetPointProvider.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\map\Map.h"
#include "qsf\component\base\TransformComponent.h"
#include "qsf\plugin\QsfJobs.h"
#include "qsf\job\JobArguments.h"
#include "qsf\debug\request\PolygonDebugDrawRequest.h"
#include "qsf\QsfHelper.h"
#include "qsf\message\MessageConfiguration.h"
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\localization\LocalizationSystem.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"

//namespace AMS start
namespace AMS
{

	//public definitions
	const uint32 TreatSkillLevelComponent::COMPONENT_ID = qsf::StringHash("AMS::TreatSkillLevelComponent");

	//Public methods

	TreatSkillLevelComponent::TreatSkillLevelComponent(qsf::Prototype* prototype) :
		qsf::Component(prototype),
		mTreatSkill(AMS::TreatSkillLevelComponent::FirstAid)
	{

	}

	TreatSkillLevelComponent::~TreatSkillLevelComponent()
	{

	}

	//protected virtual em5::Component methods

	void TreatSkillLevelComponent::starttracker(uint64 targetEntity)
	{
		TargetEntity = targetEntity;

		mJobProxy.registerAt(qsf::QsfJobs::SIMULATION, boost::bind(&TreatSkillLevelComponent::updateJob, this, _1));
	}

	bool TreatSkillLevelComponent::onStartup()
	{
		return true;
	}

	void TreatSkillLevelComponent::onShutdown()
	{
		mJobProxy.unregister();
	}

	//Private methods

	void TreatSkillLevelComponent::updateJob(const qsf::JobArguments& jobArguments)   // For Reanimation
	{
		qsf::Entity* mVictim = getEntity().getMap().getEntityById(TargetEntity);

		if (mVictim == nullptr)
		{
			mJobProxy.unregister();
			return;
		}

		em5::HealthComponent* healthComponent = mVictim->getComponent<em5::HealthComponent>();

		if (healthComponent->getLifeEnergy() > 50)
		{
			em5::HealthComponent* healthcomponent = mVictim->getComponent<em5::HealthComponent>();
			qsf::ActionComponent& actionComponent = *getEntity().getComponent<qsf::ActionComponent>();
			const TreatStabilizeAction* cheerAction = actionComponent.getAction<TreatStabilizeAction>();

			if (nullptr != cheerAction)
			{
				return;
			}

			actionComponent.clearPlan();

			if (mTreatSkill == AMS::TreatSkillLevelComponent::Notarzt)
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(getEntity(), *mVictim, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(mVictim->getId());
				actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*mVictim);

				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
			}
			else if (mTreatSkill == AMS::TreatSkillLevelComponent::LNA)
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(getEntity(), *mVictim, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(mVictim->getId());
				actionComponent.pushAction<em5::TreatPersonAction>(100, qsf::action::APPEND_TO_BACK).init(*mVictim);

				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK, true);
			}
			else
			{
				actionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachObjectGoal(getEntity(), *mVictim, em5::TreatPersonTargetPointProvider::TARGET_POINT_ID));
				actionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(mVictim->getId());
				actionComponent.pushAction<em5::PlayHintAction>(200, qsf::action::APPEND_TO_BACK).init(QT_TR_NOOP("AMS_HINT_DOCTOR_01"), em5::HintMessage::MessageType::MESSAGETYPE_HINT);
				actionComponent.pushAction<TreatStabilizeAction>(100, qsf::action::APPEND_TO_BACK).init(*mVictim);

				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS);
				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK);
				em5::StatusOverlayComponent::hideIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_EVENT_VARIOUS);
				em5::StatusOverlayComponent::showIcon(*mVictim, em5::StatusOverlayComponent::StatusIconType::STATUS_ICON_SICK_CONTAGIOUS, true);
			}

			mJobProxy.unregister();
			return;
		}

	}

}//namespace AMS end