//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\component\SpecialInjuriesComponent.h"

//Include em5
#include "em5\plugin\Messages.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\map\Map.h"
#include "qsf\plugin\QsfJobs.h"
#include "qsf\job\JobArguments.h"
#include "qsf\debug\request\PolygonDebugDrawRequest.h"
#include "qsf\QsfHelper.h"
#include "qsf\message\MessageConfiguration.h"

//Include qsf_ai

//namespace AMS start
namespace AMS
{

	//public definitions
	const uint32 SpecialInjuriesComponent::COMPONENT_ID = qsf::StringHash("AMS::SpecialInjuriesComponent");

	//Public methods

	SpecialInjuriesComponent::SpecialInjuriesComponent(qsf::Prototype* prototype) :
		qsf::Component(prototype),
		mAdipositas(AMS::SpecialInjuriesComponent::Adiposita_Deactive),
		mHWV(AMS::SpecialInjuriesComponent::HWV_Deactive),
		mIntensiv(AMS::SpecialInjuriesComponent::Intensiv_Deactive),
		mPsychological(AMS::SpecialInjuriesComponent::Psychological_normal)
	{

	}

	SpecialInjuriesComponent::~SpecialInjuriesComponent()
	{

	}

	//protected virtual em5::Component methods

	bool SpecialInjuriesComponent::onStartup()
	{
		return true;
	}

	void SpecialInjuriesComponent::onShutdown()
	{
		mJobProxy.unregister();
	}

	//Private methods

	void SpecialInjuriesComponent::updateJob(const qsf::JobArguments& jobArguments)   // For Reanimation
	{
		mJobProxy.unregister();
		return;
	}

}//namespace AMS end