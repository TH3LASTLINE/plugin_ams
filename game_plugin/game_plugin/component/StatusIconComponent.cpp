#include <game_plugin/PrecompiledHeader.h>
#include <game_plugin\component\StatusIconComponent.h>

namespace status_icon
{
	const uint32 StatusIconComponent::COMPONENT_ID = qsf::StringHash("status_icon::StatusIconComponent");

	StatusIconComponent::StatusIconComponent(qsf::Prototype* prototype) : qsf::Component(prototype),
		icon(""),
		active(true)
	{
	}

	StatusIconComponent::~StatusIconComponent()
	{
	}

	bool StatusIconComponent::onStartup()
	{
		return true;
	}

	bool StatusIconComponent::isActive()
	{
		return active;
	}

	std::string StatusIconComponent::getStatusIconType()
	{
		return icon;
	}

	void StatusIconComponent::hideStatusIcon(std::string statusIcon)
	{
		active = false;
	}

	void StatusIconComponent::showStatusIcon(std::string statusIcon)
	{
		icon = statusIcon;
		active = true;
	}

	void StatusIconComponent::hideStatusIcon(qsf::Entity* statusIconOwnerEntity, std::string statusIcon)
	{
		statusIconOwnerEntity->getOrCreateComponent<status_icon::StatusIconComponent>()->hideStatusIcon(statusIcon);
	}

	void StatusIconComponent::showStatusIcon(qsf::Entity* statusIconOwnerEntity, std::string statusIcon)
	{
		statusIconOwnerEntity->getOrCreateComponent<status_icon::StatusIconComponent>()->showStatusIcon(statusIcon);
	}
}