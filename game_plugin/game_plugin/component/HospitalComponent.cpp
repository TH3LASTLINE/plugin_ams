//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\component\HospitalComponent.h"

//Include em5
#include "em5\plugin\Messages.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\map\Map.h"
#include "qsf\plugin\QsfJobs.h"
#include "qsf\job\JobArguments.h"
#include "qsf\debug\request\PolygonDebugDrawRequest.h"
#include "qsf\QsfHelper.h"
#include "qsf\message\MessageConfiguration.h"

//Include qsf_ai

//namespace AMS start
namespace AMS
{

	//public definitions
	const uint32 HospitalComponent::COMPONENT_ID = qsf::StringHash("AMS::HospitalComponent");

	//Public methods

	HospitalComponent::HospitalComponent(qsf::Prototype* prototype) :
		qsf::Component(prototype),
		mHasShockRoom(false),
		mHasOphthalmology(false),
		mHasSurgery(false),
		mHasENTMedicine(false),
		mHasInternalMedicine(false),
		mHasCardiology(false),
		mHasPediatry(false),
		mHasNeurology(false),
		mHasPsychiatry(false),
		mHasUrology(false),
		mHasGynecology(false),
		mHasInfectiology(false)
	{

	}

	HospitalComponent::~HospitalComponent()
	{

	}

	// Shockraum
	bool HospitalComponent::getHasShockRoom() const
	{
		return mHasShockRoom;
	}

	void HospitalComponent::setHasShockRoom(bool HasShockRoom)
	{
		mHasShockRoom = HasShockRoom;
	}

	// Augenheilkunde
	bool HospitalComponent::getHasOphthalmology() const
	{
		return mHasOphthalmology;
	}

	void HospitalComponent::setHasOphthalmology(bool HasOphthalmology)
	{
		mHasOphthalmology = HasOphthalmology;
	}

	// Chirugie Unfallchirugie Orthopädie
	bool HospitalComponent::getHasSurgery() const
	{
		return mHasSurgery;
	}

	void HospitalComponent::setHasSurgery(bool HasSurgery)
	{
		mHasSurgery = HasSurgery;
	}

	// HNO Heilkunde
	bool HospitalComponent::getHasENTMedicine() const
	{
		return mHasENTMedicine;
	}

	void HospitalComponent::setHasENTMedicine(bool HasENTMedicine)
	{
		mHasENTMedicine = HasENTMedicine;
	}

	// Innere Medizin
	bool HospitalComponent::getHasInternalMedicine() const
	{
		return mHasInternalMedicine;
	}

	 void HospitalComponent::setHasInternalMedicine(bool HasInternalMedicine)
	{
		mHasInternalMedicine = HasInternalMedicine;
	}

	// Kardiologie  Chest Pain Unit
	bool HospitalComponent::getHasCardiology() const
	{
		return mHasCardiology;
	}

	void HospitalComponent::setHasCardiology(bool HasCardiology)
	{
		mHasCardiology = HasCardiology;
	}

	// Kinder Heilkunde
	bool HospitalComponent::getHasPediatry() const
	{
		return mHasPediatry;
	}

	void HospitalComponent::setHasPediatry(bool HasPediatry)
	{
		mHasPediatry = HasPediatry;
	}

	// Neurologie  Stroke Unit
	bool HospitalComponent::getHasNeurology() const
	{
		return mHasNeurology;
	}

	void HospitalComponent::setHasNeurology(bool HasNeurology)
	{
		mHasNeurology = HasNeurology;
	}

	// Psychiatrie
	bool HospitalComponent::getHasPsychiatry() const
	{
		return mHasPsychiatry;
	}

	void HospitalComponent::setHasPsychiatry(bool HasPsychiatry)
	{
		mHasPsychiatry = HasPsychiatry;
	}

	// Urologie
	bool HospitalComponent::getHasUrology() const
	{
		return mHasUrology;
	}

	void HospitalComponent::setHasUrology(bool HasUrology)
	{
		mHasUrology = HasUrology;
	}

	// Gynäkologie
	bool HospitalComponent::getHasGynecology() const
	{
		return mHasGynecology;
	}

	void HospitalComponent::setHasGynecology(bool HasGynecology)
	{
		mHasGynecology = HasGynecology;
	}

	// Infektiologie
	bool HospitalComponent::getHasInfectiology() const
	{
		return mHasInfectiology;
	}

	void HospitalComponent::setHasInfectiology(bool HasInfectiology)
	{
		mHasInfectiology = HasInfectiology;
	}

	//protected virtual em5::Component methods

	bool HospitalComponent::onStartup()
	{
		return true;
	}

	void HospitalComponent::onShutdown()
	{
		mJobProxy.unregister();
	}

	//Private methods

	void HospitalComponent::updateJob(const qsf::JobArguments& jobArguments)   // For Reanimation
	{
		mJobProxy.unregister();
		return;
	}

}//namespace AMS end