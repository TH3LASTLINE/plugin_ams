#pragma once
#include <qsf/component/Component.h>
#include <qsf\map\Entity.h>

namespace status_icon
{
	class StatusIconComponent : public qsf::Component
	{

	public:
		static const uint32 COMPONENT_ID;

		StatusIconComponent(qsf::Prototype* prototype);
		~StatusIconComponent();

		bool isActive();
		std::string getStatusIconType();
		void hideStatusIcon(std::string statusIcon);
		void showStatusIcon(std::string statusIcon);

		static void hideStatusIcon(qsf::Entity* statusIconOwnerEntity, std::string statusIcon);
		static void showStatusIcon(qsf::Entity* statusIconOwnerEntity, std::string statusIcon);

	private:
		std::string icon;
		bool active;

	protected:
		virtual bool onStartup() override;

		QSF_CAMP_RTTI()
	};
}
QSF_CAMP_TYPE_NONCOPYABLE(status_icon::StatusIconComponent)