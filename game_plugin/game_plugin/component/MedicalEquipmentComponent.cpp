//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\component\MedicalEquipmentComponent.h"

//Include em5
#include "em5\plugin\Messages.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\map\Map.h"
#include "qsf\plugin\QsfJobs.h"
#include "qsf\job\JobArguments.h"
#include "qsf\debug\request\PolygonDebugDrawRequest.h"
#include "qsf\QsfHelper.h"
#include "qsf\message\MessageConfiguration.h"

//Include qsf_ai

//namespace AMS start
namespace AMS
{

	//public definitions
	const uint32 MedicalEquipmentComponent::COMPONENT_ID = qsf::StringHash("AMS::MedicalEquipmentComponent");

	//Public methods

	MedicalEquipmentComponent::MedicalEquipmentComponent(qsf::Prototype* prototype) :
		qsf::Component(prototype),
		mMedicalEquipment(AMS::MedicalEquipmentComponent::Verbandskasten)
	{

	}

	MedicalEquipmentComponent::~MedicalEquipmentComponent()
	{

	}

	//protected virtual em5::Component methods

	bool MedicalEquipmentComponent::onStartup()
	{
		return true;
	}

	void MedicalEquipmentComponent::onShutdown()
	{
		mJobProxy.unregister();
	}

	//Private methods

	void MedicalEquipmentComponent::updateJob(const qsf::JobArguments& jobArguments)   // For Reanimation
	{
		mJobProxy.unregister();
		return;
	}

}//namespace AMS end