//Header guard
#pragma once

//Includes 
//Include AMS
#include "game_plugin\Export.h"

//Include qsf
#include "qsf\component\Component.h"
#include "qsf\job\JobProxy.h"
#include "qsf\debug\DebugDrawProxy.h"
#include "qsf\message\MessageProxy.h"

//namespace AMS start
namespace AMS
{

	//Classes
	class HospitalComponent : public qsf::Component
	{

		//public definitions
	public:

		static const uint32 COMPONENT_ID;

		//public methods
	public:

		HospitalComponent(qsf::Prototype* prototype);

		~HospitalComponent();

		// Shockraum
		bool getHasShockRoom() const;
		void setHasShockRoom(bool HasShockRoom);

		// Augenheilkunde
		bool getHasOphthalmology() const;
		void setHasOphthalmology(bool HasOphthalmology);

		// Chirugie Unfallchirugie Orthopädie
		bool getHasSurgery() const;
		void setHasSurgery(bool HasSurgery);

		// HNO Heilkunde
		bool getHasENTMedicine() const;
		void setHasENTMedicine(bool HasENTMedicine);

		// Innere Medizin
		bool getHasInternalMedicine() const;
		void setHasInternalMedicine(bool HasInternalMedicine);

		// Kardiologie  Chest Pain Unit
		bool getHasCardiology() const;
		void setHasCardiology(bool HasCardiology);

		// Kinder Heilkunde
		bool getHasPediatry() const;
		void setHasPediatry(bool HasPediatry);

		// Neurologie  Stroke Unit
		bool getHasNeurology() const;
		void setHasNeurology(bool HasNeurology);

		// Psychiatrie
		bool getHasPsychiatry() const;
		void setHasPsychiatry(bool HasPsychiatry);

		// Urologie
		bool getHasUrology() const;
		void setHasUrology(bool HasUrology);

		// Gynäkologie
		bool getHasGynecology() const;
		void setHasGynecology(bool HasGynecology);

		// Infektiologie
		bool getHasInfectiology() const;
		void setHasInfectiology(bool HasInfectiology);


		//protected virtual qsf::Component methods
	protected:

		//Lifecycle

		virtual bool onStartup() override;
		virtual void onShutdown() override;

		//private methods
	private:

		void updateJob(const qsf::JobArguments& jobArguments);

		//private data
	private:

		qsf::JobProxy mJobProxy;
		qsf::Time mTimePassed;

		uint64 TargetEntity;

		bool mHasShockRoom;
		bool mHasOphthalmology;
		bool mHasSurgery;
		bool mHasENTMedicine;
		bool mHasInternalMedicine;
		bool mHasCardiology;
		bool mHasPediatry;
		bool mHasNeurology;
		bool mHasPsychiatry;
		bool mHasUrology;
		bool mHasGynecology;
		bool mHasInfectiology;

		//CAMP reflection system
		QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::HospitalComponent)