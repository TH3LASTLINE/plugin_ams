//Header guard
#pragma once

//Includes 
//Include AMS
#include "game_plugin\Export.h"

//Include qsf
#include "qsf\component\Component.h"
#include "qsf\job\JobProxy.h"
#include "qsf\debug\DebugDrawProxy.h"
#include "qsf\message\MessageProxy.h"

//namespace AMS start
namespace AMS
{

	//Classes
	class VehicleCategoryComponent : public qsf::Component
	{

		//public definitions
	public:

		static const uint32 COMPONENT_ID;

		//public methods
	public:

		VehicleCategoryComponent(qsf::Prototype* prototype);

		~VehicleCategoryComponent();

		enum VehicleCategory	//Can be changed, only visible in the Editor
		{
			Dummy,		// Patiententransport		Adipositas			Wirbelsäulenverletzung			Intensivtransport
			NEF,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			LNA,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			ORG_L,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			MTD,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			LKW,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			MTW,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			KIT_MTW,	// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			GW_SAN,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			HvO,		// Kein Patiententransport	Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			KTW,		// Patiententransport		Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			N_KTW,		// Patiententransport		Kein Adipositas		Keine Wirbelsäulenverletzung	Kein Intensivtransport
			RTW,		// Patiententransport		Kein Adipositas		Wirbelsäulenverletzung			Kein Intensivtransport
			G_RTW,		// Multi Patiententransport	Adipositas			Wirbelsäulenverletzung			Kein Intensivtransport
			S_RTW,		// Patiententransport		Adipositas			Wirbelsäulenverletzung			Kein Intensivtransport
			ITW,		// Patiententransport		Adipositas			Wirbelsäulenverletzung			Intensivtransport
			NAW,		// Patiententransport		Adipositas			Wirbelsäulenverletzung			Intensivtransport
			RTH	,		// Patiententransport		Kein Adipositas		Wirbelsäulenverletzung			Kein Intensivtransport
			I_RTH,		// Patiententransport		Adipositas			Wirbelsäulenverletzung			Intensivtransport
			Zelt,		// Patientenaufbewahrung	Adipositas			Wirbelsäulenverletzung			Keine Intensivaufbewahrung
			Leichenwagen // "Patiententransport"	Adipositas			Wirbelsäulenverletzung			"Intensivtransport"
		};

		VehicleCategory mVehicleCategory;

		//protected virtual qsf::Component methods
	protected:

		//Lifecycle

		virtual bool onStartup() override;
		virtual void onShutdown() override;

		//private methods
	private:

		void updateJob(const qsf::JobArguments& jobArguments);

		//private data
	private:

		qsf::JobProxy mJobProxy;
		qsf::Time mTimePassed;

		uint64 TargetEntity;

		//CAMP reflection system
		QSF_CAMP_RTTI()

	};

}//namespace AMS end

 //CAMP reflection system
QSF_CAMP_TYPE(AMS::VehicleCategoryComponent::VehicleCategory)
QSF_CAMP_TYPE_NONCOPYABLE(AMS::VehicleCategoryComponent)