//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\component\VehicleCategoryComponent.h"

//Include em5
#include "em5\plugin\Messages.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\map\Map.h"
#include "qsf\plugin\QsfJobs.h"
#include "qsf\job\JobArguments.h"
#include "qsf\debug\request\PolygonDebugDrawRequest.h"
#include "qsf\QsfHelper.h"
#include "qsf\message\MessageConfiguration.h"

//Include qsf_ai

//namespace AMS start
namespace AMS
{

	//public definitions
	const uint32 VehicleCategoryComponent::COMPONENT_ID = qsf::StringHash("AMS::VehicleCategoryComponent");

	//Public methods

	VehicleCategoryComponent::VehicleCategoryComponent(qsf::Prototype* prototype) :
		qsf::Component(prototype),
		mVehicleCategory(AMS::VehicleCategoryComponent::Dummy)
	{

	}

	VehicleCategoryComponent::~VehicleCategoryComponent()
	{

	}

	//protected virtual em5::Component methods

	bool VehicleCategoryComponent::onStartup()
	{
		return true;
	}

	void VehicleCategoryComponent::onShutdown()
	{
		mJobProxy.unregister();
	}

	//Private methods

	void VehicleCategoryComponent::updateJob(const qsf::JobArguments& jobArguments)   // For Reanimation
	{
		mJobProxy.unregister();
		return;
	}

}//namespace AMS end