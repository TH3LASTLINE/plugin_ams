//Header guard
#pragma once

//Includes 
//Include AMS
#include "game_plugin\Export.h"

//Include qsf
#include "qsf\component\Component.h"
#include "qsf\job\JobProxy.h"
#include "qsf\debug\DebugDrawProxy.h"
#include "qsf\message\MessageProxy.h"

//namespace AMS start
namespace AMS
{

	//Classes
	class SpecialInjuriesComponent : public qsf::Component
	{

		//public definitions
	public:

		static const uint32 COMPONENT_ID;

		//public methods
	public:

		SpecialInjuriesComponent(qsf::Prototype* prototype);

		~SpecialInjuriesComponent();

		enum Adipositas
		{
			Adiposita_Active,
			Adiposita_Deactive
		};

		Adipositas mAdipositas;

		enum HWV
		{
			HWV_Active,
			HWV_Deactive
		};

		HWV mHWV;

		enum Intensiv
		{
			Intensiv_Active,
			Intensiv_Deactive
		};

		Intensiv mIntensiv;

		enum Psychological
		{
			Psychological_normal,
			Psychological_light,
			Psychological_medium,
			Psychological_critical
		};

		Psychological mPsychological;

		enum LNA_STATE
		{
			STATE_GREEN,
			STATE_YELLOW,
			STATE_RED,
			STATE_DEAD
		};

		LNA_STATE mLNA_STATE;

		//protected virtual qsf::Component methods
	protected:

		//Lifecycle

		virtual bool onStartup() override;
		virtual void onShutdown() override;

		//private methods
	private:

		void updateJob(const qsf::JobArguments& jobArguments);

		//private data
	private:

		qsf::JobProxy mJobProxy;
		qsf::Time mTimePassed;

		uint64 TargetEntity;

		//CAMP reflection system
		QSF_CAMP_RTTI()

	};

}//namespace AMS end

 //CAMP reflection system
QSF_CAMP_TYPE(AMS::SpecialInjuriesComponent::Adipositas)
QSF_CAMP_TYPE(AMS::SpecialInjuriesComponent::HWV)
QSF_CAMP_TYPE(AMS::SpecialInjuriesComponent::Intensiv)
QSF_CAMP_TYPE(AMS::SpecialInjuriesComponent::Psychological)
QSF_CAMP_TYPE(AMS::SpecialInjuriesComponent::LNA_STATE)
QSF_CAMP_TYPE_NONCOPYABLE(AMS::SpecialInjuriesComponent)