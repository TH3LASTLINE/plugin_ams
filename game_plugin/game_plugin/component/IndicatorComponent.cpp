//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\component\IndicatorComponent.h"
#include "game_plugin\action\GetUpWithMedikitAction.h"
#include "game_plugin\action\TreatStabilizeAction.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\component\base\TransformComponent.h"
#include "qsf\plugin\QsfJobs.h"
#include "qsf\job\JobArguments.h"
#include "qsf\debug\request\PolygonDebugDrawRequest.h"
#include "qsf\QsfHelper.h"
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\map\Map.h"

//Namespace AMS start
namespace AMS
{

	//Public definitions

		const uint32 IndicatorComponent::COMPONENT_ID = qsf::StringHash("AMS::IndicatorComponent");
		uint64 ABCDoctorBag;

	//public methods
		
		IndicatorComponent::IndicatorComponent(qsf::Prototype* prototype) :
			qsf::Component(prototype),
			ABCDoctorBag(qsf::getUninitialized<uint64>())
		{

		}

		IndicatorComponent::~IndicatorComponent()
		{

		}

	//protected virtual em5::Component methods

		bool IndicatorComponent::onStartup()
		{
			mUpdateJobProxy.registerAt(qsf::QsfJobs::SIMULATION, boost::bind(&IndicatorComponent::updateJob, this, _1));

			return true;
		}

		void IndicatorComponent::onShutdown()
		{
			mUpdateJobProxy.unregister();

		}

	//Private methods

		void IndicatorComponent::updateJob(const qsf::JobArguments& jobArguments)
		{
			qsf::ActionComponent* AC = getEntity().getComponent<qsf::ActionComponent>();

			if (AC->getAction<TreatStabilizeAction>() != nullptr && AC->getCurrentAction() == AC->getAction<TreatStabilizeAction>())
				return;

			if (getEntity().getMap().getEntityById(ABCDoctorBag) != nullptr)
				AC->pushAction<GetUpWithMedikitAction>(200, qsf::action::INSERT_AT_FRONT);

			mUpdateJobProxy.unregister();
		}

}//namespace AMS end