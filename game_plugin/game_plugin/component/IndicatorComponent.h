//Header Guard
#pragma once

//Includes
#include "game_plugin\Export.h"

#include "qsf\component\Component.h"
#include "qsf\job\JobProxy.h"
#include "qsf\debug\DebugDrawProxy.h"

// Namespace AMS start
namespace AMS
{

	//Classes

	class GAMEPLUGIN_API_EXPORT IndicatorComponent : public qsf::Component
	{

		//public definitions
		public:

			static const uint32 COMPONENT_ID;

		//public methods
		public:

			IndicatorComponent(qsf::Prototype* prototype);

			~IndicatorComponent();

			uint64 ABCDoctorBag;

		//protected virtual qsf::Component methods
		protected:

			virtual bool onStartup() override;
			virtual void onShutdown() override;

		//private methods
		private:

			void updateJob(const qsf::JobArguments& jobArguments);

		//private data
		private:

			qsf::JobProxy mUpdateJobProxy;

		//CAMP Reflection system
			QSF_CAMP_RTTI()
	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::IndicatorComponent)
