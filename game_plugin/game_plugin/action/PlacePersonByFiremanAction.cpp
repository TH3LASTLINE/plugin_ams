//Includes
//Include AMS
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/action/PlacePersonByFiremanAction.h"

//Include em5
#include "em5\map\EntityHelper.h"
#include "em5\component\effect\FadeEffectComponent.h"
#include "em5\logic\local\ReserveLogic.h"
#include "em5\logic\local\firefighters\FiremanLogic.h"
#include "em5\base\ContainerCategory.h"
#include <em5/game/selection/SelectionManager.h>
#include "em5\game\animation\AnimationHelper.h"
#include <em5/game/Game.h>
#include "em5/EM5Helper.h"
#include "em5\health\HealthComponent.h"

//Include qsf
#include <qsf/log/LogSystem.h>
#include "qsf\map\Map.h"
#include <qsf/component/base/MetadataComponent.h>
#include "qsf\logic\gamelogic\GameLogicComponent.h"

//Inlcude qsf_game
#include "qsf_game\component\base\BoneLinkComponent.h"
#include "qsf_game\component\base\BoneToBoneLinkComponent.h"

//namespace AMS start
namespace AMS
{

	//Public definitions

	const qsf::NamedIdentifier PlacePersonByFiremanAction::ACTION_ID = "AMS::PlacePersonByFiremanAction";


	//Public methods

	PlacePersonByFiremanAction::PlacePersonByFiremanAction() :
		qsf::Action(ACTION_ID)
	{
		// Nothing here
	}

	PlacePersonByFiremanAction::~PlacePersonByFiremanAction()
	{
		// Nothing here
	}

	void PlacePersonByFiremanAction::init(qsf::Entity* TargetEntity)
	{
		mTargetEntity = TargetEntity;
	}

	//Protected virtual qsf::Action methods

	bool PlacePersonByFiremanAction::onStartup()
	{
		mState = STATE_ONE;
		return true;
	}

	qsf::action::Result PlacePersonByFiremanAction::updateAction(const qsf::Clock& clock)
	{
		switch (mState)
		{
		case STATE_ONE:
		{
			qsf::Entity* mCaller = &getEntity();
			em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();
			mFEC->fadeOut();

			mState = STATE_TWO;
			return qsf::action::RESULT_CONTINUE;
		}break;

		case STATE_TWO:
		{
			qsf::Entity* mCaller = &getEntity();
			em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();

			if (!mFEC->isFading())
			{
				em5::FiremanLogic* mFL = em5::EntityHelper(mCaller).getGameLogic<em5::FiremanLogic>();

				uint64 mVictimID = mFL->getPullingPersonId();
				qsf::Map* mMap = &mCaller->getMap();
				qsf::Entity* mVictim = mMap->getEntityById(mVictimID);

				em5::EntityHelper(mVictim).unlinkFromParent();

				em5::ReserveParamedicLogic::deleteOwnReservation(mVictim, getEntityId());

				em5::EntityHelper victimentityhelper(mVictim);

				mCaller->getComponent<qsf::GameLogicComponent>()->deleteGameLogic(*em5::EntityHelper(mCaller).getGameLogic<em5::FiremanLogic>());

				mCaller->getComponent<qsf::GameLogicComponent>()->createGameLogic<em5::FiremanLogic>();

				em5::HealthComponent* mHC = mVictim->getComponent<em5::HealthComponent>();

				mHC->setLifeEnergy(mHC->getMaximumLifeEnergy());
				mHC->setDiagnosisComplete(true);
				mHC->setInjuredHealed();

				em5::AnimationHelper animationHelperVictim(*mVictim);

				animationHelperVictim.clearAnimation();
				animationHelperVictim.playAnimation(animationHelperVictim.getAnimationVictimOnStretcher(), true);

				victimentityhelper.enterContainer(*mTargetEntity, em5::container::ContainerTypes::CONTAINERTYPE_PASSENGER);

				mFEC->fadeIn();

				mState = STATE_THREE;
			}

			return qsf::action::RESULT_CONTINUE;
		}break;

		case STATE_THREE:
		{
			qsf::Entity* mCaller = &getEntity();
			em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();

			if (!mFEC->isFading())
			{
				return qsf::action::RESULT_DONE;
			}

			return qsf::action::RESULT_CONTINUE;
		}break;
		}

		// Error
		return qsf::action::RESULT_DONE;
	}

} //namespace AMS 