//Includes
//Include AMS
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/action/GetPersonFromVehicleAction.h"

//Include em5
#include <em5/component/vehicle/VehicleComponent.h>
#include "em5\component\effect\FadeEffectComponent.h"
#include "em5\base\ContainerCategory.h"
#include <em5/game/Game.h>
#include "em5/EM5Helper.h"
#include "em5\map\EntityHelper.h"
#include "em5\health\HealthHelper.h"
#include "em5\health\HealthComponent.h"
#include "em5\logic\local\ambulance\ParamedicLogic.h"

//Include qsf
#include <qsf/component/base/MetadataComponent.h>
#include "qsf\component\link\LinkComponent.h"
#include "qsf\link\LinkProxy.h"
#include "qsf\link\link\prototype\ContainerLink.h"

//Inlcude qsf_game
#include "qsf_game\component\base\BoneLinkComponent.h"
#include "qsf_game\component\base\PrototypeContainer.h"
#include "qsf_game\component\vehicle\VehicleComponent.h"

//namespace AMS start
namespace AMS
{

	//Public definitions

	const qsf::NamedIdentifier GetPersonFromVehicleAction::ACTION_ID = "AMS::GetPersonFromVehicleAction";


	//Public methods

	GetPersonFromVehicleAction::GetPersonFromVehicleAction() :
		qsf::Action(ACTION_ID)
	{
		// Nothing here
	}

	GetPersonFromVehicleAction::~GetPersonFromVehicleAction()
	{
		// Nothing here
	}

	void GetPersonFromVehicleAction::init(qsf::Entity* TargetEntity)
	{
		mTargetEntity = TargetEntity;
	}

	//Protected virtual qsf::Action methods

	bool GetPersonFromVehicleAction::onStartup()
	{
		mState = STATE_ONE;
		return true;
	}

	qsf::action::Result GetPersonFromVehicleAction::updateAction(const qsf::Clock& clock)
	{

		switch (mState)
		{

			case STATE_ONE:
			{			
				//EM5_GAME.getSelectionManager().clearSelection();

				qsf::Entity* mCaller = &getEntity();
				em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();
				mFEC->fadeOut();

				mState = STATE_TWO;
				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_TWO:
			{
				qsf::Entity* mStretcher;
				qsf::Entity* mCaller = &getEntity();
				em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();

				if (!mFEC->isFading())
				{
					qsf::Entity* mCaller;
					mCaller = &getEntity();

					qsf::LinkComponent* mLC = mCaller->getComponent<qsf::LinkComponent>();

					for (auto iterator : mLC->getChildLinks())
					{
						qsf::Entity& linkedEntity = iterator->getEntity();
						qsf::Entity* mTargetEntity = &linkedEntity;

						qsf::MetadataComponent* mMC = mTargetEntity->getComponent<qsf::MetadataComponent>();

						if (mMC != nullptr)
						{
							std::string tag = mMC->getTags();

							if (tag == "stretcher")
							{
								mStretcher = mTargetEntity;
							}
						}
					}
					
					em5::VehicleComponent* mVehicleComponent = mTargetEntity->getComponent<em5::VehicleComponent>();
					const qsf::LinkProxy* wrongLinkProxyPointer = &mVehicleComponent->getLinkProxy();
					const qsf::LinkProxy* rightLinkProxyPointer = reinterpret_cast<const qsf::LinkProxy*>(reinterpret_cast<const char*>(wrongLinkProxyPointer) - 8);
					auto& links = reinterpret_cast<const boost::container::flat_map<qsf::LinkAnchorId, qsf::ContainerLink*>&>(rightLinkProxyPointer->getLinkConnectionMap());

					std::vector<qsf::Entity*> mVec;

					for (auto iterator : links)
					{
						const qsf::ContainerLink& containerLink = *iterator.second;

						if (containerLink.mContainerCategory == em5::container::CONTAINERTYPE_PASSENGER)
						{
							mVec.push_back(&containerLink.getTargetEntity());
						}
					}
					
					qsf::Entity* mVictim = mVec.front();

					em5::EntityHelper victimentityhelper(mVictim);
					victimentityhelper.leaveContainer();

					em5::HealthComponent* mHealthComponent = mVictim->getComponent<em5::HealthComponent>();
					em5::HealthHelper* mHealthHelper = &em5::HealthHelper(*mHealthComponent);

					mHealthHelper->linkToStretcher(*mStretcher);

					em5::ParamedicLogic* mCPL = em5::EntityHelper(mCaller).getGameLogic<em5::ParamedicLogic>();

					mCPL->setCarryPerson(mVictim->getId());

					mFEC->fadeIn();

					mState = STATE_THREE;
				}

				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_THREE:
			{
				qsf::Entity* mCaller = &getEntity();
				em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();

				if (!mFEC->isFading())
				{
					return qsf::action::RESULT_DONE;
				}

				return qsf::action::RESULT_CONTINUE;
			}break;
		}

		// Error
		return qsf::action::RESULT_DONE;
	}

} //namespace AMS end