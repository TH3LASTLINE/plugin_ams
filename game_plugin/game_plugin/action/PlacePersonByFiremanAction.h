//Header guard
#pragma once

//Includes
//Inlcude qsf
#include <qsf/logic/action/Action.h>

//Forward declarations
namespace qsf
{
	class MeshAnimationChannel;
}

//namespace AMS start
namespace AMS
{

	//classes
	class PlacePersonByFiremanAction : public qsf::Action
	{

		//public definitions
	public:

		static const qsf::NamedIdentifier ACTION_ID;

		//public methods
	public:

		PlacePersonByFiremanAction();

		virtual ~PlacePersonByFiremanAction();

		void init(qsf::Entity* TargetEntity);

		//Protected virtual qsf::Action methods
	protected:

		virtual bool onStartup() override;

		qsf::action::Result PlacePersonByFiremanAction::updateAction(const qsf::Clock& clock);

		//private data
	private:

		enum State
		{
			STATE_ONE,
			STATE_TWO,
			STATE_THREE
		};

		State mState;
		qsf::Entity* mTargetEntity;

		//CAMP reflection syste,
		QSF_CAMP_RTTI()

	};

}//namespace AMS end

 //CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::PlacePersonByFiremanAction)
