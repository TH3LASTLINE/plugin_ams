//Header guard
#pragma once

//Includes
//Inlcude qsf
#include <qsf/logic/action/Action.h>

//Forward declarations
namespace qsf
{
	class MeshAnimationChannel;
}

//namespace AMS start
namespace AMS
{

	//classes
	class GetPersonFromVehicleAction : public qsf::Action
	{

		//public definitions
	public:

		static const qsf::NamedIdentifier ACTION_ID;

		//public methods
	public:

		GetPersonFromVehicleAction();

		virtual ~GetPersonFromVehicleAction();

		void init(qsf::Entity* TargetEntity);

		//Protected virtual qsf::Action methods
	protected:

		virtual bool onStartup() override;

		qsf::action::Result GetPersonFromVehicleAction::updateAction(const qsf::Clock& clock);

		//private data
	private:

		enum State
		{
			STATE_ONE,
			STATE_TWO,
			STATE_THREE
		};

		State mState;
		qsf::Entity* mTargetEntity;

		//CAMP reflection syste,
		QSF_CAMP_RTTI()

	};

}//namespace AMS end

 //CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::GetPersonFromVehicleAction)
