//Header guard
#pragma once

//Includes
#include "qsf\logic\action\Action.h"

//Forward declarations

//Namespace qsf start
namespace qsf
{

	class MeshAnimationChannel;

}//namespace qsf end

//namespace AMS start
namespace AMS
{

	//Classes

	class TreatStabilizeAction : public qsf::Action
	{

		//public definitions

		public:

			static const qsf::NamedIdentifier ACTION_ID;

		public:

			TreatStabilizeAction();

			virtual ~TreatStabilizeAction();

			void init(qsf::Entity& targetEntity);

		//Protected virtual qsf::action methods

		protected:

			virtual bool onStartup() override;
			virtual void onShutdown() override;

			virtual qsf::action::Result updateAction(const qsf::Clock& clock) override;

		//private data

		private:

			enum State
			{
				STATE_INIT,
				STATE_PUT_DOWN_DOCTORBAG,
				STATE_TURN_TO_PATIENT,
				STATE_DIAGNOSIS,
				STATE_DEAD
			};

			State mState;

			qsf::MeshAnimationChannel* mCheerAnimationChannel;

			int mRepeat;
			float mLifeEnergy;

			qsf::WeakPtr<qsf::Entity> mTargetEntity;

			//qsf::Entity* mTargetEntity;
			qsf::Entity* mDoctorBag;

			//CAMP reflection system

			QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system

QSF_CAMP_TYPE_NONCOPYABLE(AMS::TreatStabilizeAction)
