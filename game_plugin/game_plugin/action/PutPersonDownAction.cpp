//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\action\PutPersonDownAction.h"

//Include em5
#include "em5\map\EntityHelper.h"
#include "em5\logic\local\ambulance\ParamedicLogic.h"
#include "em5\logic\local\ReserveLogic.h"
#include "em5\game\animation\AnimationHelper.h"
#include "em5/game/groundmap/GroundMaps.h"

//Include qsf
#include "qsf\QsfHelper.h"
#include "qsf\link\link\Link.h"
#include "qsf\component\link\LinkComponent.h"
#include "qsf\map\Map.h"
#include <qsf/map/query/GroundMapQuery.h>
#include <qsf/map/query/RayMapQuery.h>
#include <qsf/link/link/prototype/ContainerLink.h>
#include "qsf\component\base\MetadataComponent.h"
#include "qsf\renderer\animation\MeshAnimationChannel.h"
#include "qsf\component\base\TransformComponent.h"
#include "qsf\component\link\LinkComponent.h"
#include "qsf\logic\gamelogic\GameLogicComponent.h"

//Inlcude qsf_game
#include "qsf_game\component\base\BoneLinkComponent.h"
#include "qsf_game\component\base\BoneToBoneLinkComponent.h"

//Namespace AMS start
namespace AMS
{

	//public definitions

	const qsf::NamedIdentifier PutPersonDownAction::ACTION_ID = "AMS::PutPersonDownAction";

	//Public methods

	PutPersonDownAction::PutPersonDownAction() :
		qsf::Action(ACTION_ID),
		mState(STATE_ONE),
		mAnimationParaFrontChannel(nullptr),
		mAnimationParaBackChannel(nullptr),
		mAnimationStretcherChannel(nullptr)
	{

	}

	PutPersonDownAction::~PutPersonDownAction()
	{

	}

	//protected virtual qsf::Action methods

	bool PutPersonDownAction::onStartup()
	{
		return true;
	}

	qsf::action::Result PutPersonDownAction::updateAction(const qsf::Clock& clock)
	{

		switch (mState)
		{			
			
			case STATE_ONE:
			{
				qsf::Entity* mCaller;
				mCaller = &getEntity();

				qsf::LinkComponent* mLC = mCaller->getComponent<qsf::LinkComponent>();

				for (auto iterator : mLC->getChildLinks())
				{
					qsf::Entity& linkedEntity = iterator->getEntity();
					qsf::Entity* mTargetEntity = &linkedEntity;

					qsf::MetadataComponent* mMC = mTargetEntity->getComponent<qsf::MetadataComponent>();

					if (mMC != nullptr)
					{
						std::string tag = mMC->getTags();

						if (tag == "stretcher")
						{
							mStretcher = mTargetEntity;
						}

						if (tag == "first_paramedic")
						{
							mParaFront = mTargetEntity;
						}

						if (tag == "second_paramedic")
						{
							mParaBack = mTargetEntity;
						}
					}
				}

				mState = STATE_TWO;
				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_TWO:
			{
				em5::AnimationHelper animationHelperstretcher(*mStretcher);
				em5::AnimationHelper animationHelperparafront(*mParaFront);
				em5::AnimationHelper animationHelperparaback(*mParaBack);

				animationHelperstretcher.clearAnimation();
				animationHelperparafront.clearAnimation();
				animationHelperparaback.clearAnimation();

				mAnimationStretcherChannel = animationHelperstretcher.playAnimation(animationHelperstretcher.getAnimationEquipmentStretcherLower());
				mAnimationParaFrontChannel = animationHelperparafront.playAnimation(animationHelperparafront.getAnimationParamedicLowerStretcherFront());
				mAnimationParaBackChannel = animationHelperparaback.playAnimation(animationHelperparaback.getAnimationParamedicLowerStretcherBack());		

				mState = STATE_THREE;

				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_THREE:
			{
				if (nullptr != mAnimationStretcherChannel && mAnimationStretcherChannel->isFinished())
				{
					if (nullptr != mAnimationParaFrontChannel && mAnimationParaFrontChannel->isFinished())
					{
						if (nullptr != mAnimationParaBackChannel && mAnimationParaBackChannel->isFinished())
						{
							qsf::Entity* mCaller;
							mCaller = &getEntity();

							em5::ParamedicLogic* mCPL = em5::EntityHelper(mCaller).getGameLogic<em5::ParamedicLogic>();

							uint64 mVictimID = mCPL->getCarryPerson();
							qsf::Map* mMap = &mCaller->getMap();
							qsf::Entity* mVictim = mMap->getEntityById(mVictimID);

							em5::EntityHelper(mVictim).unlinkFromParent();

							em5::ReserveParamedicLogic::deleteOwnReservation(mVictim, getEntityId());

							qsf::TransformComponent* mTC = mVictim->getComponent<qsf::TransformComponent>();

							glm::vec3 mPos = mTC->getPosition();

							float groundHeight = 0.f;

							qsf::GroundMapQuery(QSF_MAINMAP, em5::GroundMaps::FILTER_DEFAULT).getHeightAt(mPos, groundHeight);

							mPos.y = groundHeight;

							glm::vec3 mPosChange(0.8, 0, 0);

							mPos = mPos + mPosChange;

							mTC->setPosition(mPos);

							mCaller->getComponent<qsf::GameLogicComponent>()->deleteGameLogic(*em5::EntityHelper(mCaller).getGameLogic<em5::ParamedicLogic>());
							mCaller->getComponent<qsf::GameLogicComponent>()->createGameLogic<em5::ParamedicLogic>();

							mState = STATE_FOUR;
						}
					}
				}

				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_FOUR:
			{
				if (nullptr != mAnimationStretcherChannel && mAnimationStretcherChannel->isFinished())
				{
					if (nullptr != mAnimationParaFrontChannel && mAnimationParaFrontChannel->isFinished())
					{
						if (nullptr != mAnimationParaBackChannel && mAnimationParaBackChannel->isFinished())
						{
							em5::AnimationHelper animationHelperstretcher(*mStretcher);
							em5::AnimationHelper animationHelperparafront(*mParaFront);
							em5::AnimationHelper animationHelperparaback(*mParaBack);

							mAnimationStretcherChannel = animationHelperstretcher.playAnimation(animationHelperstretcher.getAnimationEquipmentStretcherRaise());
							mAnimationParaFrontChannel = animationHelperparafront.playAnimation(animationHelperparafront.getAnimationParamedicRaiseStretcherFront());
							mAnimationParaBackChannel = animationHelperparaback.playAnimation(animationHelperparaback.getAnimationParamedicRaiseStretcherBack());

							mState = STATE_FIVE;
						}
					}
				}

				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_FIVE:
			{
				if (nullptr != mAnimationStretcherChannel && mAnimationStretcherChannel->isFinished())
				{
					if (nullptr != mAnimationParaFrontChannel && mAnimationParaFrontChannel->isFinished())
					{
						if (nullptr != mAnimationParaBackChannel && mAnimationParaBackChannel->isFinished())
						{
							return qsf::action::RESULT_DONE;
						}
					}
				}

				return qsf::action::RESULT_CONTINUE;
			}break;

		}

		//Error
		return qsf::action::RESULT_DONE;
	}

}//Namespace AMS end