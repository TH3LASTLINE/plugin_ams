//Header guard
#pragma once

//Includes
#include "qsf\logic\action\Action.h"

//Namespace qsf start
namespace qsf
{
	class MeshAnimationChannel;
} //Namespace qsf end

  //Namespace AMS start
namespace AMS
{

	//classes
	
	class PutPersonDownAction : public qsf::Action
	{

		//Public definitions
		public:

			static const qsf::NamedIdentifier ACTION_ID;

		//Public methods
		public:

			PutPersonDownAction();

			virtual ~PutPersonDownAction();

		//protected virtual qsf::Acton methods
		protected:

			virtual bool onStartup() override;

			virtual qsf::action::Result updateAction(const qsf::Clock& clock) override;

		//private data
		private:

			enum State
			{
				STATE_ONE,
				STATE_TWO,
				STATE_THREE,
				STATE_FOUR,
				STATE_FIVE
			};

			State mState;
			qsf::MeshAnimationChannel* mAnimationParaFrontChannel;
			qsf::MeshAnimationChannel* mAnimationParaBackChannel;
			qsf::MeshAnimationChannel* mAnimationStretcherChannel;

			qsf::Entity* mStretcher;
			qsf::Entity* mParaFront;
			qsf::Entity* mParaBack;

		// CAMP reflection system
		QSF_CAMP_RTTI()
	};

}//Namespace AMS end

//Camp reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::PutPersonDownAction)