//Header guard
#pragma once

//Includes
//Inlcude qsf
#include <qsf/logic/action/Action.h>

//Include em5
#include <em5/component/door/DoorComponent.h>

//Forward declarations
namespace qsf
{
	class MeshAnimationChannel;
}

//namespace AMS start
namespace AMS
{

	//classes
	class OpenCloseDoorAction : public qsf::Action
	{

	//public definitions
	public:
		
		static const qsf::NamedIdentifier ACTION_ID;

	//public methods
	public:

		OpenCloseDoorAction();

		virtual ~OpenCloseDoorAction();

		void init(qsf::Entity* entity, em5::DoorComponent::DoorType doortype, bool open);

	//Protected virtual qsf::Action methods
	protected:

		virtual bool onStartup() override;

		qsf::action::Result OpenCloseDoorAction::updateAction(const qsf::Clock& clock);

	//private data
	private:

		qsf::Entity* mEntity;

		em5::DoorComponent::DoorType mDoorType;

		bool mOpen;

	//CAMP reflection syste,
	QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::OpenCloseDoorAction)
