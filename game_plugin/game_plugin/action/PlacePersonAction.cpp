//Includes
//Include AMS
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/action/PlacePersonAction.h"

//Include em5
#include "em5\map\EntityHelper.h"
#include "em5\component\effect\FadeEffectComponent.h"
#include "em5\logic\local\ReserveLogic.h"
#include "em5\logic\local\ambulance\ParamedicLogic.h"
#include "em5\base\ContainerCategory.h"
#include <em5/game/selection/SelectionManager.h>
#include <em5/game/Game.h>
#include "em5/EM5Helper.h"

//Include qsf
#include <qsf/log/LogSystem.h>
#include "qsf\map\Map.h"
#include <qsf/component/base/MetadataComponent.h>
#include "qsf\logic\gamelogic\GameLogicComponent.h"

//Inlcude qsf_game
#include "qsf_game\component\base\BoneLinkComponent.h"
#include "qsf_game\component\base\BoneToBoneLinkComponent.h"

//namespace AMS start
namespace AMS
{

	//Public definitions

	const qsf::NamedIdentifier PlacePersonAction::ACTION_ID = "AMS::PlacePersonAction";


	//Public methods

	PlacePersonAction::PlacePersonAction() :
		qsf::Action(ACTION_ID)
	{
		// Nothing here
	}

	PlacePersonAction::~PlacePersonAction()
	{
		// Nothing here
	}

	void PlacePersonAction::init(qsf::Entity* TargetEntity)
	{
		mTargetEntity = TargetEntity;
	}

	//Protected virtual qsf::Action methods

	bool PlacePersonAction::onStartup()
	{
		mState = STATE_ONE;
		return true;
	}

	qsf::action::Result PlacePersonAction::updateAction(const qsf::Clock& clock)
	{
		switch (mState)
		{
			case STATE_ONE:
			{
				qsf::Entity* mCaller = &getEntity();
				em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();
				mFEC->fadeOut();

				mState = STATE_TWO;
				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_TWO:
			{
				qsf::Entity* mCaller = &getEntity();
				em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();

				if (!mFEC->isFading())
				{
					em5::ParamedicLogic* mCPL = em5::EntityHelper(mCaller).getGameLogic<em5::ParamedicLogic>();

					uint64 mVictimID = mCPL->getCarryPerson();
					qsf::Map* mMap = &mCaller->getMap();
					qsf::Entity* mVictim = mMap->getEntityById(mVictimID);
					
					em5::EntityHelper(mVictim).unlinkFromParent();

					em5::ReserveParamedicLogic::deleteOwnReservation(mVictim, getEntityId());

					em5::EntityHelper victimentityhelper(mVictim);

					mCaller->getComponent<qsf::GameLogicComponent>()->deleteGameLogic(*em5::EntityHelper(mCaller).getGameLogic<em5::ParamedicLogic>());

					mCaller->getComponent<qsf::GameLogicComponent>()->createGameLogic<em5::ParamedicLogic>();

					victimentityhelper.enterContainer(*mTargetEntity, em5::container::ContainerTypes::CONTAINERTYPE_PASSENGER);

					mFEC->fadeIn();

					mState = STATE_THREE;
				}

				return qsf::action::RESULT_CONTINUE;
			}break;

			case STATE_THREE:
			{
				qsf::Entity* mCaller = &getEntity();
				em5::FadeEffectComponent* mFEC = mCaller->getOrCreateComponent<em5::FadeEffectComponent>();

				if (!mFEC->isFading())
				{
					return qsf::action::RESULT_DONE;
				}

				return qsf::action::RESULT_CONTINUE;
			}break;
		}

		// Error
		return qsf::action::RESULT_DONE;
	}

} //namespace AMS 