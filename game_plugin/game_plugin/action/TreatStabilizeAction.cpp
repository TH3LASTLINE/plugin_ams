//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\action\TreatStabilizeAction.h"
#include "game_plugin\component\IndicatorComponent.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\command\CommandContext.h"
#include "em5\game\animation\AnimationHelper.h"
#include "em5\map\EntityHelper.h"
#include "em5\health\HealthComponent.h"
#include "em5\action\base\PlayHintAction.h"
#include "em5\action\base\PlayAudioAction.h"
#include "em5\logic\local\ReserveLogic.h"
#include "em5\component\overlay\StatusOverlayComponent.h"

//Include qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\renderer\animation\MeshAnimationChannel.h"
#include "qsf\message\MessageSystem.h"
#include "qsf\QsfHelper.h"
#include "qsf\reflection\type\CampGlmQuat.h"
#include "qsf\math\EulerAngles.h"
#include "qsf\log\LogSystem.h"
#include "qsf\component\base\MetadataComponent.h"
#include "qsf\renderer\mesh\MeshComponent.h"
#include "qsf\reflection\type\CampQsfAssetProxy.h"
#include "qsf\component\link\LinkComponent.h"

//Include qsf_game
#include "qsf_game\command\CommandManager.h"
#include "qsf_game\command\CommandSystem.h"
#include "qsf_game\command\CommandContext.h"
#include "qsf_game\equipment\EquipmentComponent.h"
#include "qsf_game\QsfGameHelper.h"

//namespace AMS start
namespace AMS
{

	//public definitions

	const qsf::NamedIdentifier TreatStabilizeAction::ACTION_ID = "AMS::TreatStabilizeAction";

	//public methods

	TreatStabilizeAction::TreatStabilizeAction() :
		qsf::Action(ACTION_ID),
		mState(STATE_INIT),
		mCheerAnimationChannel(nullptr)
	{

	}

	TreatStabilizeAction::~TreatStabilizeAction()
	{

	}

	void TreatStabilizeAction::init(qsf::Entity& targetEntity)
	{
		/*mVictim = Victim;

		if (em5::ReserveLogic::checkReservation(*Victim, getEntityId()))
		{
			em5::ReserveLogic::createReservationByTypeId(*Victim, getEntityId(), qsf::StringHash("em5::ReserveDoctorLogic"));
		}*/
		mTargetEntity = &targetEntity;

		if (em5::ReserveLogic::checkReservation(mTargetEntity.getSafe(), getEntityId()))
		{
			em5::ReserveLogic::createReservation<em5::ReserveMoveToTargetLogic>(mTargetEntity.getSafe(), getEntityId());
		}
	}

	//protected virtual qsf::Action methods

	bool TreatStabilizeAction::onStartup()
	{
		em5::CommandContext commandContext;

		commandContext.mCaller = &getEntity();
		commandContext.mAllowSelfExecution = true;

		if(!em5::ReserveLogic::checkReservation(mTargetEntity.getSafe(), getEntityId()))
			return false;

		em5::HealthComponent* healthComponent = mTargetEntity->getComponent<em5::HealthComponent>();
		mLifeEnergy = healthComponent->getLifeEnergy();

		return true;
	}

	void TreatStabilizeAction::onShutdown()
	{
		em5::AnimationHelper animationHelper(getEntity());
		animationHelper.clearAnimation();

		em5::ReserveLogic::deleteOwnReservation(*mTargetEntity, getEntityId());
	}

	qsf::action::Result TreatStabilizeAction::updateAction(const qsf::Clock& clock)
	{
		em5::HealthComponent* healthComponent = mTargetEntity->getComponent<em5::HealthComponent>();
		healthComponent->setLifeEnergy(mLifeEnergy);

		switch (mState)
		{

		case STATE_INIT:
		{
			em5::ReserveLogic::deleteOwnReservation(getMap(), mTargetEntity->getId(), getEntityId());
			em5::ReserveLogic::createReservation<em5::ReserveDoctorLogic>(mTargetEntity.getSafe(), getEntityId());

			if (em5::EntityHelper(getEntity()).getActiveEquipmentEntity() != nullptr)
			{
				IndicatorComponent* IC = getEntity().getOrCreateComponent<IndicatorComponent>();
				qsf::Entity* mDoctorBag = em5::EntityHelper(getEntity()).getActiveEquipmentEntity();
				qsf::Entity* DoctorBag = em5::EntityHelper(getEntity()).getActiveEquipmentEntity();
				em5::AnimationHelper animationHelper(getEntity());

				IC->ABCDoctorBag = mDoctorBag->getId();

				mCheerAnimationChannel = animationHelper.playAnimation(animationHelper.getAnimationDoctorGetDown());
				DoctorBag->getComponent<qsf::MeshComponent>()->setMesh(qsf::AssetProxy("em5/mesh/equipment/equipment_emergency_medic_case_open"));

				mState = STATE_PUT_DOWN_DOCTORBAG;
			}
			else
			{
				em5::AnimationHelper animationHelper(getEntity());
				mCheerAnimationChannel = animationHelper.playAnimation(animationHelper.getAnimationDoctorGetDown());

				mState = STATE_PUT_DOWN_DOCTORBAG;
			}

			return qsf::action::RESULT_CONTINUE;
		}

		case STATE_PUT_DOWN_DOCTORBAG:
		{

			if (nullptr != mCheerAnimationChannel && mCheerAnimationChannel->isFinished())
			{
				if (em5::EntityHelper(getEntity()).getActiveEquipmentEntity() != nullptr)
				{
					qsf::Entity* DoctorBag = em5::EntityHelper(getEntity()).getActiveEquipmentEntity();
					em5::AnimationHelper animationHelper(getEntity());
					em5::AnimationHelper animationHelperBag(*DoctorBag);

					DoctorBag->getComponent<qsf::LinkComponent>()->unlinkFromParent();

					mCheerAnimationChannel = animationHelper.playAnimation(animationHelper.getAnimationDoctorOpenMedicCase());
					qsf::MeshAnimationChannel* DoctorBagAnimationChannel = animationHelperBag.playAnimation(animationHelperBag.getAnimationEquipmentOpenMedikit());

					mState = STATE_TURN_TO_PATIENT;
				}
				else
				{
					mState = STATE_TURN_TO_PATIENT;
				}
			}

			return qsf::action::RESULT_CONTINUE;
		}

		case STATE_TURN_TO_PATIENT:
		{
			if (nullptr != mCheerAnimationChannel && mCheerAnimationChannel->isFinished())
			{
				em5::AnimationHelper animationHelper(getEntity());

				mCheerAnimationChannel = animationHelper.playAnimation("em5/skeleton/doctor/doctor_put_down_bag_03");

				mState = STATE_DIAGNOSIS;
			}

			return qsf::action::RESULT_CONTINUE;
		}

		case STATE_DIAGNOSIS:
		{
			if (nullptr != mCheerAnimationChannel && mCheerAnimationChannel->isFinished())
			{
				if (mLifeEnergy > 0)
				{
					em5::AnimationHelper animationHelper(getEntity());
					mCheerAnimationChannel = animationHelper.playAnimation(animationHelper.getAnimationDoctorDiagnosis(), true);
				}
				else
				{
					em5::AnimationHelper animationHelper(getEntity());
					mCheerAnimationChannel = animationHelper.playAnimation(animationHelper.getAnimationDoctorDiagnosis());

					mState = STATE_DEAD;
				}
			}

			return qsf::action::RESULT_CONTINUE;
		}

		case STATE_DEAD:
		{
			if (nullptr != mCheerAnimationChannel && mCheerAnimationChannel->isFinished())
				return qsf::action::RESULT_DONE;
		}
		
		}//switch case end

		//Error
		return qsf::action::RESULT_DONE;
	}

}//namespace AMS end