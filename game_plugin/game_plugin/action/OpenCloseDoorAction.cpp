//Includes
//Include AMS
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/action/OpenCloseDoorAction.h"

//Include qsf
#include <qsf/log/LogSystem.h>
#include <qsf/component/base/MetadataComponent.h>

//Include em5
#include <em5/component/vehicle/VehicleComponent.h>

//namespace AMS start
namespace AMS
{
	
	//Public definitions
	
	const qsf::NamedIdentifier OpenCloseDoorAction::ACTION_ID = "AMS::OpenCloseDoorAction";


	//Public methods

	OpenCloseDoorAction::OpenCloseDoorAction() :
		qsf::Action(ACTION_ID)
	{
		// Nothing here
	}

	OpenCloseDoorAction::~OpenCloseDoorAction()
	{
		// Nothing here
	}

	void OpenCloseDoorAction::init(qsf::Entity* entity, em5::DoorComponent::DoorType doortype, bool open)
	{
		mEntity = entity;
		mDoorType = doortype;
		mOpen = open;
	}

	//Protected virtual qsf::Action methods

	bool OpenCloseDoorAction::onStartup()
	{
		return true;
	}

	qsf::action::Result OpenCloseDoorAction::updateAction(const qsf::Clock& clock)
	{
		mEntity->getComponent<em5::VehicleComponent>()->openDoor(mDoorType, mOpen);
		return qsf::action::RESULT_DONE;
	}

} //namespace AMS end