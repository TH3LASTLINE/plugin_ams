//Includes
//Includes AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\action\GetUpWithMedikitAction.h"
#include "game_plugin\component\IndicatorComponent.h" 

//Includes qsf
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\renderer\animation\MeshAnimationChannel.h"
#include "qsf\message\MessageSystem.h"
#include "qsf\QsfHelper.h"
#include "qsf\component\link\LinkComponent.h"
#include "qsf\reflection\type\CampQsfAssetProxy.h"
#include "qsf\renderer\mesh\MeshComponent.h"
#include "qsf\log\LogSystem.h"
#include "qsf\component\base\MetadataComponent.h"

//Includes qsf_game
#include "qsf_game\command\CommandManager.h"
#include "qsf_game\command\CommandSystem.h"
#include "qsf_game\equipment\EquipmentComponent.h"
#include "qsf_game\QsfGameHelper.h"

//Includes em5
#include "em5\action\ActionPriority.h"
#include "em5\command\CommandContext.h"
#include "em5\game\animation\AnimationHelper.h"
#include "em5\map\EntityHelper.h"
#include "em5\action\base\PlayHintAction.h"

//Namespace AMS start
namespace AMS
{

	//public defnitions

		const qsf::NamedIdentifier GetUpWithMedikitAction::ACTION_ID = "AMS::GetUpWithMedikitAction";

	//public methods

		GetUpWithMedikitAction::GetUpWithMedikitAction() :
			qsf::Action(ACTION_ID),
			mState(STATE_INIT),
			mCheerAnimationChannel(nullptr),
			mDoctorBagAnimationChannel(nullptr)
		{
			//	Nothing here
		}

		GetUpWithMedikitAction::~GetUpWithMedikitAction()
		{
			// Nothing here
		}

	//protected virtual qsf::Action methods

		bool GetUpWithMedikitAction::onStartup()
		{
			return true;
		}

		qsf::action::Result GetUpWithMedikitAction::updateAction(const qsf::Clock& clock)
		{
			qsf::Entity* mDoctorBag = em5::EntityHelper(getEntity()).getActiveEquipmentEntity();
			switch (mState)
			{
				case STATE_INIT:
				{
					getEntity().destroyComponent<IndicatorComponent>();
					em5::AnimationHelper animationHelper(getEntity());
					mCheerAnimationChannel = animationHelper.playAnimation("em5/skeleton/doctor/doctor_put_down_bag_03", false, true);
					mState = STATE_GET_DOCTORBAG;
					return qsf::action::RESULT_CONTINUE;
				}break;
				case STATE_GET_DOCTORBAG:
				{
					em5::AnimationHelper animationHelper(getEntity());
					em5::AnimationHelper animationHelperBag(*mDoctorBag);
					if (nullptr != mCheerAnimationChannel && mCheerAnimationChannel->isFinished())
					{
						mCheerAnimationChannel = animationHelper.playAnimation(animationHelper.getAnimationDoctorOpenMedicCase(), false, true);
						mDoctorBagAnimationChannel = animationHelperBag.playAnimation(animationHelperBag.getAnimationEquipmentOpenMedikit(), false, true);
						mState = STATE_GET_UP;
					}
					return qsf::action::RESULT_CONTINUE;
				}break;
				case STATE_GET_UP:
				{
					em5::AnimationHelper animationHelper(getEntity());
					if (nullptr != mDoctorBagAnimationChannel && mDoctorBagAnimationChannel->isFinished())
					{
						if (nullptr != mCheerAnimationChannel && mCheerAnimationChannel->isFinished())
						{
							mDoctorBag->getComponent<qsf::LinkComponent>()->linkToParent(getEntity());
							mCheerAnimationChannel = animationHelper.playAnimation(animationHelper.getAnimationDoctorGetDown(), false, true);
							mDoctorBag->getComponent<qsf::MeshComponent>()->setMesh(qsf::AssetProxy("em5/mesh/equipment/equipment_emergency_medic_case_closed"));
							mState = STATE_FINISH;
						}
					}
					return qsf::action::RESULT_CONTINUE;
				}break;
				case STATE_FINISH:
				{
					if (nullptr != mCheerAnimationChannel && mCheerAnimationChannel->isFinished())
					{
						return qsf::action::RESULT_DONE;
					}
					return qsf::action::RESULT_CONTINUE;
				}break;
			}

			//Error
			return qsf::action::RESULT_DONE;
		}

}//Namespace AMS end