//Header guard
#pragma once

//Includes
#include "qsf\logic\action\Action.h"

//Namespace qsf start
namespace qsf
{
	class MeshAnimationChannel;
} //Namespace qsf end

//Namespace AMS start
namespace AMS
{

	// classes

	class GetUpWithMedikitAction : public qsf::Action
	{
		//Public definitions
		public:

			static const qsf::NamedIdentifier ACTION_ID;

		//Public methods
		public:

			GetUpWithMedikitAction();

			virtual ~GetUpWithMedikitAction();

		//protected virtual qsf::Acton methods
		protected:

			virtual bool onStartup() override;

			virtual qsf::action::Result updateAction(const qsf::Clock& clock) override;

		//private data
		private:

			enum State
			{
				STATE_INIT,
				STATE_GET_DOCTORBAG,
				STATE_GET_UP,
				STATE_FINISH
			};

			State mState;
			qsf::MeshAnimationChannel* mCheerAnimationChannel;
			qsf::MeshAnimationChannel* mDoctorBagAnimationChannel;
			std::string control;

		// CAMP reflection system
		QSF_CAMP_RTTI()

	};

} //Namespace AMS end

//Camp reflection system
QSF_CAMP_TYPE_NONCOPYABLE(AMS::GetUpWithMedikitAction)