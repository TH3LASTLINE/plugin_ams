//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\commandmode\TentDisplacementCommandmode.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\base\ContainerCategory.h"
#include "em5/component/vehicle/VehicleComponent.h"
#include "em5\logic\HintHelper.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\map\EntityHelper.h"
#include "qsf\link\LinkProxy.h"
#include "qsf\link\link\prototype\ContainerLink.h"

//Include qsf_game
#include "qsf_game\command\CommandContext.h"

//namespace AMS start
namespace AMS
{

	//public defintions
	const uint32 TentDisplacementCommandmode::PLUGINABLE_ID = qsf::StringHash("AMS::TentDisplacementCommandmode");

	//private defintions
	const uint32 TentDisplacementCommandmode::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods
	TentDisplacementCommandmode::TentDisplacementCommandmode() :
		qsf::game::CommandMode(PLUGINABLE_ID),
		mMap(nullptr)
	{

	}

	TentDisplacementCommandmode::~TentDisplacementCommandmode()
	{

	}

	void TentDisplacementCommandmode::initialize(qsf::Entity* callerEntity)
	{
		if (nullptr != callerEntity)
		{
			mCommandVehicle = callerEntity;
			mMap = &callerEntity->getMap();
		}
	}

	//public virtual qsf::game::Commandmode methods
	void TentDisplacementCommandmode::startup()
	{

	}

	void TentDisplacementCommandmode::shutdown()
	{

	}

	bool TentDisplacementCommandmode::executeCommandContext(qsf::game::CommandContext& commandContext, const EntityIdSet& callerIds)
	{
		std::string mTargetEntityStringName = qsf::EntityHelper(*commandContext.mTargetEntity).getName();
		uint64 mTargetEntityId = commandContext.mTargetEntity->getId();

		if (mTargetEntityStringName != "seg_bhp_12_zelt")
			return false;

		em5::VehicleComponent* mVehicleComponent = commandContext.mTargetEntity->getComponent<em5::VehicleComponent>();
		const qsf::LinkProxy* wrongLinkProxyPointer = &mVehicleComponent->getLinkProxy();
		const qsf::LinkProxy* rightLinkProxyPointer = reinterpret_cast<const qsf::LinkProxy*>(reinterpret_cast<const char*>(wrongLinkProxyPointer) - 8);
		auto& links = reinterpret_cast<const boost::container::flat_map<qsf::LinkAnchorId, qsf::ContainerLink*>&>(rightLinkProxyPointer->getLinkConnectionMap());

		std::vector<qsf::Entity*> mVec;

		for (auto iterator : links)
		{
			const qsf::ContainerLink& containerLink = *iterator.second;

			if (containerLink.mContainerCategory == em5::container::CONTAINERTYPE_PASSENGER)
			{
				mVec.push_back(&containerLink.getTargetEntity());
			}
		}

		if (mVec.size() > 0)
		{
			em5::HintHelper::showHint("Ist das Zelt wirklich leer?", 0, qsf::Time::fromSeconds(0.0f));
		}
		else 
		{
			mMap->qsf::Map::destroyObjectById(mTargetEntityId);
		}

		return true;
	}

	void TentDisplacementCommandmode::updateCommandMode(const qsf::Clock& clock)
	{

	}

}//namespace AMS end